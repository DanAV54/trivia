#include "RoomMemberRequestHandler.h"

/*
Constructor
input: (RoomManager&)		    roomMan- value to roomManager
	   (RequestHandlerFactory&) reqHand- value to RequestHandlerFactory
	   (LoggedUser)				user- value to loggedUser
	   (Room*)					room- value to room
*/
RoomMemberRequestHandler::RoomMemberRequestHandler(RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user, Room* room) : m_handlerFactory(reqHand), m_roomManager(roomMan), m_user(user), m_room(room)
{
}

/*
Checks if The Request is Relevent- Relevant Request is a req that matches the state the client is in now (Menu and so on)
input:  (RequestInfo) req- the request info keep the id inside and if the id matches room stuff so true
output: (Bool)        true/false- relevent/not relevent
*/
bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
	return req.id == LEAVE_ROOM_CODE || req.id == GET_STATE_CODE || req.id == CHANGE_HANDLER;
}

/*
Handle Request, and bridging between Code to Func, and return their result
input:  (RequestInfo)   req- the data of the login request type (log/sign)
output: (RequestResult) reqRes- the result from login/signup
*/
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;

	if (req.id == LEAVE_ROOM_CODE)
	{
		reqRes = this->leaveRoom(req);
	}
	else if (req.id == GET_STATE_CODE)
	{
		reqRes = this->getRoomState(req);
	}
	else if (req.id == CHANGE_HANDLER)
	{
		reqRes = this->changeHandler(req);
	}

	return reqRes;
}

/*
leave the Room, if it succeeded so send success
input:  (RequestInfo)   req- the data of the leaveRoom request
output: (RequestResult) reqRes- the result from leaveRoom
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo req)
{
	RequestResult     reqRes;
	LeaveRoomResponse leaveRes;
	bool              flag;

	(*this->m_room).removeUser(this->m_user);

	leaveRes.status   = POSITIVE_STATUS;
	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(leaveRes);
	reqRes.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);
	return reqRes;
}

/*
get the room state, if it succeeded so send success and the room state
input:  (RequestInfo)   req- the data of the getRoomState request
output: (RequestResult) reqRes- the result from getRoomState
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo req)
{
	std::vector<std::string> temp;
	RequestResult			 reqRes;
	GetRoomStateResponse	 stateRes;
	
	stateRes.hasGameBegun  = m_roomManager.getRoomState((*m_room).getRoomId());
	if (stateRes.hasGameBegun == 0)
	{
		stateRes.players = temp;
		stateRes.questionCount = 0;
		stateRes.status = NEGATIVE_STATUS;
		stateRes.answerTimeout = 0;
	}
	else
	{
		stateRes.players = (*m_room).getAllUsers();
		stateRes.questionCount = (*m_room).getRoomQuestionCount();
		stateRes.status = POSITIVE_STATUS;
		stateRes.answerTimeout = (*m_room).getRoomTimePerQuestion();
	}

	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(stateRes);
	reqRes.newHandler = nullptr;
	return reqRes;
}

/*
switching to game handler
input:  (RequestInfo)   req- the data of the startPlaying request
output: (RequestResult) reqRes- the result from startPlaying
*/
RequestResult RoomMemberRequestHandler::changeHandler(RequestInfo req)
{
	RequestResult		  reqRes;
	ChangeHandlerRequest  chanReq;
	ChangeHandlerResponse chanRes;

	chanReq = JsonRequestPacketDeserializer::deserializeChangeHandlerRequest(req.buffer);
	if (chanReq.newHandler == 1)
	{
		reqRes.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(m_user);
	}
	else if (chanReq.newHandler == 2)
	{
		reqRes.newHandler = (IRequestHandler*)m_handlerFactory.createGameRequestHandler(m_user, this->m_handlerFactory.getGameManager().getGame(this->m_room->getRoomId()));
	}

	chanRes.status  = POSITIVE_STATUS;
	reqRes.response = JsonResponsePacketSerializer::serializeResponse(chanRes);
	return reqRes;
}
