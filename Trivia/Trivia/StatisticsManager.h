#pragma once
#include <vector>
#include <string>
#include <map>
#include "LoginStructs.h"
#include "IDatabase.h"
class StatisticsManager
{
public:
	//Constructor
	StatisticsManager() = default;

	//Destructor
	~StatisticsManager() = default;

	//Setters
	void setDataBase(IDatabase* database);

	//Getters
	std::vector<std::pair<std::string, float>> getHighScore();
	std::map<std::string, float>			   getUserStatistics(std::string username);
	void									   addQuestion(std::string question, std::string currAns, std::string ans2, std::string ans3, std::string ans4);
	void									   submitQuestion(Statistic statics);

private:
	//Properties
	IDatabase* m_database;

	//Getters
	std::vector<std::pair<std::string, float>> getScores();
};