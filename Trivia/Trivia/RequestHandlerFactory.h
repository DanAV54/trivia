#pragma once
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	//Constructor
	RequestHandlerFactory(IDatabase* dataBase);

	//Destructor
	~RequestHandlerFactory();

	//Methods
	LoginRequestHandler*	  createLoginRequestHandler();
	MenuRequestHandler*		  createMenuRequestHandler(LoggedUser user);
	RoomAdminRequestHandler*  createRoomAdminRequestHandler(LoggedUser user, Room* room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, Room* room);
	GameRequestHandler*       createGameRequestHandler(LoggedUser user, Game* game);

	//Getters
	LoginManager&      getLoginManager();
	StatisticsManager& getStatisticsManager();
	RoomManager&	   getRoomManager();
	GameManager&	   getGameManager();

private:
	//Properties
	IDatabase*		  m_database;
	LoginManager	  m_loginManager;
	RoomManager		  m_roomManager;
	StatisticsManager m_StatisticsManager;
	GameManager		  m_gameManager;
};
