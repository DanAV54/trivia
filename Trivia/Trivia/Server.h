#pragma once
#include "Communicator.h"
#include "SqliteDatabase.h"
#include "RequestHandlerFactory.h"
#include "WSAInitializer.h"

class Server
{
public:
	//Constructor
	Server();

	//Destructor
	~Server();

	//Methods
	void run();

private:
	//Properties
	IDatabase*			  m_database;
	WSAInitializer		  wsainitializer;
	Communicator		  m_communicator;
	RequestHandlerFactory m_handlerFactory;
};
