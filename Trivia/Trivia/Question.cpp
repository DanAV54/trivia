#include "Question.h"

/*
Constructor
input: (QuestionStruct) q- value to question
*/
Question::Question(QuestionStruct q)
{
	m_question = q.question;
	m_currAns = q.currAns;
	m_possibleAnswers.push_back(q.ans2);
	m_possibleAnswers.push_back(q.ans3);
	m_possibleAnswers.push_back(q.ans4);
}

/*
get the question
input:  (void)
output: (string) m_question- the question
*/
std::string Question::getQuestion()
{
	return m_question;
}

/*
get the answers
input:  (void)
output: (vector<string>) m_possibleAnswers- the answers
*/
std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

/*
get the correct answer
input:  (void)
output: (string) m_currAns- the correct answer
*/
std::string Question::getCorrentAnswer()
{
	return m_currAns;
}

/*
get the data as a struct
input:  (void)
output: (QuestionStruct) [untitled]- the struct
*/
QuestionStruct Question::getStruct()
{
	return QuestionStruct{ m_question, m_currAns, m_possibleAnswers[0], m_possibleAnswers[1], m_possibleAnswers[2] };
}
