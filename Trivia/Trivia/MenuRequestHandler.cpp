#include "MenuRequestHandler.h"

/*
Constructor
input: (StatisticsManager&)		statMan- value to staticsManager
	   (RoomManager&)			roomMan- value to roomManager
	   (RequestHandlerFactory&) reqHand- value to RequestHandlerFactory
	   (LoggedUser)				user- value to loggedUser
*/
MenuRequestHandler::MenuRequestHandler(StatisticsManager& statMan, RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user): m_staticsManager(statMan), m_roomManager(roomMan), m_handlerFactory(reqHand), m_loggedUser(user)
{
}

/*
Checks if The Request is Relevent- Relevant Request is a req that matches the state the client is in now (Menu and so on)
input:  (RequestInfo) req- the request info keep the id inside and if the id matches room stuff so true
output: (Bool)        true/false- relevent/not relevent
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo req)
{
	if (req.id != LOGOUT_CODE		  &&
		req.id != GET_ROOM			  &&
		req.id != GET_PLAYERS_IN_ROOM &&
		req.id != JOIN_ROOM			  &&
		req.id != CREATE_ROOM		  &&
		req.id != GET_HIGH_SCORE	  &&
		req.id != ADD_QUESTION		  &&
		req.id != GET_PERSONAL_STATS  )
	{
		return false;
	}
	return true;
}

/*
Handle Request, and bridging between Code to Func, and return their result
input:  (RequestInfo)   req- the data of the login request type (log/sign)
output: (RequestResult) reqRes- the result from login/signup
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;

	if (req.id == LOGOUT_CODE)
	{
		reqRes = this->logout(req);
	}
	else if (req.id == GET_ROOM)
	{
		reqRes = this->getRooms(req);
	}
	else if (req.id == GET_PLAYERS_IN_ROOM)
	{
		reqRes = this->getPlayersInRoom(req);
	}
	else if (req.id == JOIN_ROOM)
	{
		reqRes = this->joinRoom(req);
	}
	else if (req.id == CREATE_ROOM)
	{
		reqRes = this->createRoom(req);
	}
	else if (req.id == GET_HIGH_SCORE)
	{
		reqRes = this->getHighScore(req);
	}
	else if (req.id == GET_PERSONAL_STATS)
	{
		reqRes = this->getPersonalStats(req);
	}
	else if (req.id == ADD_QUESTION)
	{
		reqRes = this->addQuestion(req);
	}

	return reqRes;
}

/*
logout the user, if it succeeded so send to previous statment & send success
input:  (RequestInfo)   req- the data of the logout request
output: (RequestResult) reqRes- the result from logout
*/
RequestResult MenuRequestHandler::logout(RequestInfo req)
{
	RequestResult                 reqRes;
	LogoutResponse                logRes;
	bool                          flag;

	flag = this->m_handlerFactory.getLoginManager().logout(this->m_loggedUser.getUsername());
	if (flag)
	{
		logRes.status	  = POSITIVE_STATUS;
		reqRes.response   = JsonResponsePacketSerializer::serializeResponse(logRes);
		reqRes.newHandler = (IRequestHandler*)this->m_handlerFactory.createLoginRequestHandler();
	}
	else
	{
		logRes.status	  = NEGATIVE_STATUS;
		reqRes.response   = JsonResponsePacketSerializer::serializeResponse(logRes);
		reqRes.newHandler = nullptr;
	}
	return reqRes;
}

/*
return room list, if it succeeded send success and rooms
input:  (RequestInfo)   req- the data of the getRoom request
output: (RequestResult) reqRes- the result from getRoom
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo req)
{
	RequestResult                 reqRes;
	GetRoomsResponse              getRoomsRes;
	bool                          flag;
	
	getRoomsRes.rooms  = this->m_roomManager.getRooms();
	getRoomsRes.status = POSITIVE_STATUS;
	reqRes.response    = JsonResponsePacketSerializer::serializeResponse(getRoomsRes);
	reqRes.newHandler  = nullptr;
	return reqRes;
}

/*
return players in a certain room list, if it succeeded send success and players
input:  (RequestInfo)   req- the data of the getPlayersInRoom request
output: (RequestResult) reqRes- the result from getPlayersInRoom
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo req)
{
	GetPlayersInRoomRequest       GetPlaInRoomReq;
	GetPlayersInRoomResponse      GetPlaInRoomRes;
	RequestResult                 reqRes;
	bool                          flag;

	GetPlaInRoomReq			= JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(req.buffer);
	GetPlaInRoomRes.players = (*m_roomManager.getRoom(GetPlaInRoomReq.roomId)).getAllUsers();
	reqRes.response			= JsonResponsePacketSerializer::serializeResponse(GetPlaInRoomRes);
	reqRes.newHandler		= nullptr;
	return reqRes;

}

/*
return player's personal stats, if it succeeded send success and stats
input:  (RequestInfo)   req- the data of the getPersonalStats request
output: (RequestResult) reqRes- the result from getPersonalStats
*/
RequestResult MenuRequestHandler::getPersonalStats(RequestInfo req)
{
	GetPersonalStatsResponse      GetPerStaRes;
	RequestResult                 reqRes;
	bool                          flag;

	
	std::map<std::string, float> userStatics = m_staticsManager.getUserStatistics(this->m_loggedUser.getUsername());

	GetPerStaRes.statics.push_back(std::to_string(int(userStatics["correctAnswers"])));
	GetPerStaRes.statics.push_back(std::to_string(int(userStatics["playerGames"])));
	GetPerStaRes.statics.push_back(std::to_string(int(userStatics["totalAnswers"])));
	GetPerStaRes.statics.push_back(std::to_string(userStatics["averageTime"]).substr(0, 4));

	GetPerStaRes.status  = POSITIVE_STATUS;
	reqRes.response		 = JsonResponsePacketSerializer::serializeResponse(GetPerStaRes);
	reqRes.newHandler	 = nullptr;
	return reqRes;
}

/*
return highScore, if it succeeded send success and highScore
input:  (RequestInfo)   req- the data of the getHighScore request
output: (RequestResult) reqRes- the result from getHighScore
*/
RequestResult MenuRequestHandler::getHighScore(RequestInfo req)
{
	std::vector<std::string>	  strHighScore;
	JsonResponsePacketSerializer  jsonser;
	GetHighScoreResponse          getHigScoRes;
	RequestResult                 reqRes;
	bool                          flag;

	auto highScore = m_staticsManager.getHighScore();

	for (auto it = highScore.begin(); it != highScore.end(); it++)
	{
		strHighScore.push_back(it->first + SPLIT_CHAR + std::to_string(it->second));
	}

	getHigScoRes.statics = strHighScore;
	getHigScoRes.status  = POSITIVE_STATUS;
	reqRes.response		 = JsonResponsePacketSerializer::serializeResponse(getHigScoRes);
	reqRes.newHandler	 = nullptr;
	return reqRes;
}

/*
joins certain Room, if it succeeded send success
input:  (RequestInfo)   req- the data of the joinRoom request
output: (RequestResult) reqRes- the result from joinRoom
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo req)
{
	JoinRoomRequest	 joinRoomReq;
	JoinRoomResponse joinRoomRes;
	RequestResult    reqRes;

	joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(req.buffer);
	Room* room  = m_roomManager.getRoom(joinRoomReq.roomId);

	if ((*room).addUser(this->m_loggedUser))
	{
		joinRoomRes.status = POSITIVE_STATUS;
	}
	else
	{
		joinRoomRes.status = NEGATIVE_STATUS;
	}

	reqRes.response    = JsonResponsePacketSerializer::serializeResponse(joinRoomRes);
	reqRes.newHandler  = (IRequestHandler*)this->m_handlerFactory.createRoomMemberRequestHandler(this->m_loggedUser, room);
	return reqRes;
}

/*
Create Room, if it succeeded send success and roomId
input:  (RequestInfo)   req- the data of the createRoom request
output: (RequestResult) reqRes- the result from createRoom
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo req)
{
	RequestResult      reqRes;
	CreateRoomRequest  creRoomReq;
	CreateRoomResponse creRoomRes;
	bool               flag;
	RoomData		   roomData;
	Room*              roomUsed;

	creRoomReq					  = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(req.buffer);
	roomData.timePerQuestion	  = creRoomReq.answerTimeOut;
	roomData.maxPlayers			  = creRoomReq.maxUsers;
	roomData.numOfQuestionsInGame = creRoomReq.questionCount;
	roomData.name				  = creRoomReq.roomName;
	
	roomUsed	      = this->m_roomManager.createRoom(this->m_loggedUser, roomData);
	creRoomRes.roomId = (*roomUsed).getRoomData().id;
	creRoomRes.status = POSITIVE_STATUS;
	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(creRoomRes);
	reqRes.newHandler = (IRequestHandler*)this->m_handlerFactory.createRoomAdminRequestHandler(this->m_loggedUser, roomUsed);

	return reqRes;
}

/*
add a question
input:  (RequestInfo)   req- the data of the addQuestion request
output: (RequestResult) reqRes- the result from addQuestion
*/
RequestResult MenuRequestHandler::addQuestion(RequestInfo req)
{
	AddQuestionRequest	addQuestReq;
	AddQuestionResponse addQuestRes;
	RequestResult       reqRes;

	addQuestReq        = JsonRequestPacketDeserializer::deserializeAddQuestionRequest(req.buffer);
	this->m_staticsManager.addQuestion(addQuestReq.question, addQuestReq.currAns, addQuestReq.ans2, addQuestReq.ans3, addQuestReq.ans4);
	addQuestRes.status = POSITIVE_STATUS;

	reqRes.response = JsonResponsePacketSerializer::serializeResponse(addQuestRes);
	reqRes.newHandler = nullptr;
	return reqRes;
}
