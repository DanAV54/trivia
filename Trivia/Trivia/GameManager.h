#pragma once
#include <vector>
#include <map>
#include "Room.h"
#include "Game.h"
#include "IDatabase.h"
class GameManager
{
public:
	//Constructor
	GameManager() = default;
	GameManager(IDatabase* datebase);

	//Destructor
	~GameManager() = default;

	//Methods
	Game* createGame(Room* room);
	void  deleteGame(Game* game);

	//Getters
	std::vector<PlayerResults> getResults(Game* game);
	Game*					   getGame(unsigned int id);

private:
	//Properties
	IDatabase*		   m_database;
	std::vector<Game*> m_games;
};