#include "LoginManager.h"

/*
Constructor
input: (IDatabase*) database- new value to m_database
*/
LoginManager::LoginManager(IDatabase* datebase)
{
	this->m_database = datebase;
}

/*
Destructor
input: none
*/
LoginManager::~LoginManager()
{
}

/*
Signup a user, add his details to the Database and add him to the LoggedUsers
input:  (String) username- name to add to the Database
	    (String) password- password to add to the Database
	    (String) email- email to add to the Database
output: (Bool)   true/false- if succeedd to signup
*/
bool LoginManager::signup(std::string username, std::string password, std::string email)
{	
	if (this->m_database->addNewUser(username, password, email))
	{
		this->m_loggedUsers.push_back(LoggedUser(username));
		return true;
	}
	else
	{
		return false;
	}
}

/*
Login a user, check if his details in the database and adds him to the LoggedUsers
input:  (String) username- name to match with password
		(String) password- password to match with name
output: (Bool)   true/false- if succeedd to login
*/
bool LoginManager::login(std::string username, std::string password)
{
	try {
		if (m_database->doesPasswordMatch(username, password))
		{
			for (auto it = m_loggedUsers.begin(); it != m_loggedUsers.end(); it++)
			{
				if (it->getUsername() == username)
				{
					return false;
				}
			}
			m_loggedUsers.push_back(LoggedUser(username));
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
		return false;
	}
}

/*
Logout a user, check if his details in the logged users and remove him from it
input:  (String) username- name to remove
output: (Bool)   true/false- if succeedd to logout
*/
bool LoginManager::logout(std::string username)
{
	for (auto it = m_loggedUsers.begin(); it != m_loggedUsers.end(); it++)
	{
		if (it->getUsername() == username)
		{
			m_loggedUsers.erase(it);
			return true;
		}
	}
	return false;
}

/*
Setter to m_database
input:  (IDatabase*) datebase- new value to database
output: void
*/
void LoginManager::setDatabase(IDatabase* datebase)
{
	this->m_database = datebase;
}