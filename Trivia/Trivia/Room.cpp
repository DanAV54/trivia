#include "Room.h"

/*
Constructor
input: (RoomData) roomData- value to m_metadata
*/
Room::Room(RoomData roomData)
{
	m_metadata = roomData;
}

/*
Add User To Room
input:  (LoggedUser) user- user to add
output: (bool)       true/false- if succeeded to add
*/
bool Room::addUser(LoggedUser user)
{
	if (this->m_metadata.maxPlayers == this->m_users.size())
	{
		return false;
	}
	m_users.push_back(user);
	return true;
}

/*
remove User From Room
input:  (LoggedUser) user- user to remove
output: (void)
*/
void Room::removeUser(LoggedUser user)
{
	if (!m_users.empty())
	{
		for (auto it = m_users.begin(); it != m_users.end(); it++)
		{
			if (it->getUsername() == user.getUsername())
			{
				m_users.erase(it);
				break;
			}
		}
	}
}

/*
Setters to active
input:  (bool) active- new value to m_metadata.isActive 
output: (void)
*/
void Room::setActive(int active)
{
	this->m_metadata.isActive = active;
}

/*
getter to user's name
input:  (void)
output: (std::vector<std::string>) usernames- player's name
*/
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> usersNames;

	if (!m_users.empty())
	{
		for (auto it = m_users.begin(); it != m_users.end(); it++)
		{
			usersNames.push_back(it->getUsername());
		}
	}
	return usersNames;
}

/*
getter to room name
input:  (void)
output: (std::string) m_metadata.name- room name
*/
std::string Room::getRoomName()
{
	return this->m_metadata.name;
}

/*
getter to room id
input:  (void)
output: (unsigned int) m_metadata.id- room id
*/
unsigned int Room::getRoomId()
{
	return this->m_metadata.id;
}

/*
getter to room maxPlayers
input:  (void)
output: (unsigned int) m_metadata.maxPlayers- room maxPlayers
*/
unsigned int Room::getRoomMaxUsers()
{
	return this->m_metadata.maxPlayers;
}

/*
getter to room isActive
input:  (void)
output: (unsigned int) m_metadata.isActive- room activity
*/
unsigned int Room::getRoomActivity()
{
	return this->m_metadata.isActive;
}

/*
getter to room numOfQuestionsInGame
input:  (void)
output: (unsigned int) m_metadata.numOfQuestionsInGame- room numOfQuestionsInGame
*/
unsigned int Room::getRoomQuestionCount()
{
	return this->m_metadata.numOfQuestionsInGame;
}

/*
getter to room timePerQuestion
input:  (void)
output: (unsigned int) m_metadata.timePerQuestion- room timePerQuestion
*/
unsigned int Room::getRoomTimePerQuestion()
{
	return this->m_metadata.timePerQuestion;
}

/*
getter to room data
input:  (void)
output: (RoomData) m_metadata- room data
*/
RoomData Room::getRoomData()
{
	return this->m_metadata;
}