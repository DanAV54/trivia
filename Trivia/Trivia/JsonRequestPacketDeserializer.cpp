#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"
using namespace nlohmann;

//VERSION 1

/*
Parse The Buffer and Deserializing it into a LoginRequest
input:  (unsigned int vector) buffer- the buffer to parse and create from
output: (LoginRequest)        loginReq- the result from the desrializing
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
    LoginRequest loginReq;

    json j_complete   = json::parse(buffer);
    loginReq.username = j_complete["username"];
    loginReq.password = j_complete["password"];

    return loginReq;
}

/*
Parse The Buffer and Deserializing it into a SignupRequest
input:  (unsigned int vector) buffer- the buffer to parse and create from
output: (SignupRequest)       signupReq- the result from the desrializing
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
    SignupRequest signupReq;

    json j_complete    = json::parse(buffer);
    signupReq.username = j_complete["username"];
    signupReq.password = j_complete["password"];
    signupReq.email    = j_complete["email"];

    return signupReq;
}

//VERSION 2

/*
Parse The Buffer and Deserializing it into a GetPlayersInRoomRequest
input:  (unsigned int vector)     buffer- the buffer to parse and create from
output: (GetPlayersInRoomRequest) getPlInRo- the result from the desrializing
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer)
{
    GetPlayersInRoomRequest getPlInRo;

    json j_complete  = json::parse(buffer);
    getPlInRo.roomId = j_complete["roomId"];

    return getPlInRo;
}

/*
Parse The Buffer and Deserializing it into a JoinRoomRequest
input:  (unsigned int vector) buffer- the buffer to parse and create from
output: (JoinRoomRequest)     joiRoo- the result from the desrializing
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
    JoinRoomRequest joiRoo;

    json j_complete = json::parse(buffer);
    joiRoo.roomId   = j_complete["roomId"];
    
    return joiRoo;
}

/*
Parse The Buffer and Deserializing it into a CreateRoomRequests
input:  (unsigned int vector) buffer- the buffer to parse and create from
output: (CreateRoomRequest)   creRoo- the result from the desrializing
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
    CreateRoomRequest creRoo;

    json j_complete      = json::parse(buffer);
    creRoo.answerTimeOut = j_complete["answerTimeOut"];
    creRoo.maxUsers      = j_complete["maxUsers"];
    creRoo.questionCount = j_complete["questionCount"];
    creRoo.roomName      = j_complete["roomName"];

    return creRoo;
}

/*
Parse The Buffer and Deserializing it into a AddQuestionRequest
input:  (unsigned int vector) buffer- the buffer to parse and create from
output: (AddQuestionRequest)  addQuest- the result from the desrializing
*/
AddQuestionRequest JsonRequestPacketDeserializer::deserializeAddQuestionRequest(std::vector<unsigned char> buffer)
{
    AddQuestionRequest addQuest;

    json j_complete   = json::parse(buffer);
    addQuest.question = j_complete["question"];
    addQuest.currAns  = j_complete["currAns"];
    addQuest.ans2     = j_complete["ans2"];
    addQuest.ans3     = j_complete["ans3"];
    addQuest.ans4     = j_complete["ans4"];

    return addQuest;
}

//VERSION 3

/*
Parse The Buffer and Deserializing it into a ChangeHandlerRequest
input:  (unsigned int vector)  buffer- the buffer to parse and create from
output: (ChangeHandlerRequest) chanHand- the result from the desrializing
*/
ChangeHandlerRequest JsonRequestPacketDeserializer::deserializeChangeHandlerRequest(std::vector<unsigned char> buffer)
{
    ChangeHandlerRequest chanHand;

    json j_complete     = json::parse(buffer);
    chanHand.newHandler = j_complete["newHandler"];

    return chanHand;
}

//VERSION 4

/*
Parse The Buffer and Deserializing it into a SubmitAnswerRequest
input:  (unsigned int vector)  buffer- the buffer to parse and create from
output: (SubmitAnswerRequest)  subReq- the result from the desrializing
*/
SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer)
{
    SubmitAnswerRequest subReq;

    json j_complete = json::parse(buffer);
    subReq.answerId = j_complete["answerId"];
    subReq.timer    = j_complete["timer"];

    return subReq;
}
