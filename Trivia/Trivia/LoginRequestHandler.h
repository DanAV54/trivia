#pragma once
#include "IRequestHandler.h"
#include "MenuRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include <vector>

//OTHER DEFINES
#define POSITIVE_STATUS      1
#define NEGATIVE_STATUS      0

class RequestHandlerFactory;

class LoginRequestHandler : IRequestHandler
{
public:
	//Constructor
	LoginRequestHandler(LoginManager& LogMan, RequestHandlerFactory& ReqHand);

	//Destructor
	~LoginRequestHandler();

	//Methods
	bool		  isRequestRelevant(RequestInfo req) override;
	RequestResult handleRequest(RequestInfo req)     override;

private:
	//Properties
	LoginManager&          m_loginManager;
	RequestHandlerFactory& m_handlerFactory;
	LoggedUser			   m_loggedUser;

	//Methods
	RequestResult login(RequestInfo);
	RequestResult signup(RequestInfo);
};
