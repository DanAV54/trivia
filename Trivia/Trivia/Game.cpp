#include "Game.h"


/*
Constructor
input: (std::vector<Question> questions) questions- gameQuestions
	   (std::map<LoggedUser, GameData>)  players - gamePlayers
	   (IDatabase*)						 DB - database for statistics add
	   (int)							 gameId - the id of the game
*/
Game::Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players, IDatabase* DB, int gameId) : m_players(players), m_questions(questions), m_dataBase(DB), m_gameId(gameId)
{
	for (auto it = m_players.begin(); it != m_players.end(); ++it)
	{
		m_waiting.insert(std::pair<LoggedUser, bool>(LoggedUser(it->first),false));
	}
	m_questionNum = 0;
}

/*
Get the Current Question for the User
input:  (LoggedUser)	 user- the user to get for
output: (QuestionStruct) [untitled]- the question
*/
QuestionStruct Game::getQuestionForUser(LoggedUser user)
{
	return this->m_players[user].currentQuestion;
}

/*
submit the answer and update data
input:  (LoggedUser) user- the user to submit for
		(string)     answer- the answer the user submited
		(float)      time- the time it took
output: (void)
*/
void Game::submitAnswer(LoggedUser user, std::string answer, float time)
{
	auto currentQuestion = m_players[user].currentQuestion;
	if (answer == currentQuestion.currAns)
		m_players[user].correctAnswerCount++;
	else
		m_players[user].wrongAnswerCount++;
	m_players[user].averangeAnswerTime = (m_players[user].averangeAnswerTime + time) / 2;
	m_dataBase->addUserData(Statistic{ m_questionNum, answer == currentQuestion.currAns, time , user.getUsername()});
}

/*
remove player from game
input:  (LoggedUser) user- the user to remove
output: (void)
*/
void Game::removePlayer(LoggedUser user)
{
	m_players.erase(m_players.find(user));
	m_waiting.erase(m_waiting.find(user));
}

/*
get the players
input:  (void)
output: (std::map<LoggedUser, GameData>) m_players- the players
*/
std::map<LoggedUser, GameData> Game::getPlayers()
{
	return m_players;
}

/*
operator == (not very useful but errors...)
input:  (Game&) other- the game to compare
output: (bool)  always false
*/
bool Game::operator==(const Game& other) const
{
	return false;
}

/*
get the game id
input:  (void)
output: (int) m_gameId- the id
*/
int Game::getGameId()
{
	return this->m_gameId;
}

/*
notify the user is waiting for question and check if the other already did
input:  (LoggedUser) log- the user to notify for
output: (bool)		 flag- if everybody notified
*/
bool Game::notify(LoggedUser log)
{
	bool flag = true;
	for (auto it = m_players.begin(); it != m_players.end(); ++it)
	{
		if (LoggedUser(it->first).getUsername() == log.getUsername())
		{
			m_waiting[log] = true;
		}
		else
		{
			if (flag)
			{
				flag = m_waiting[it->first];
			}
		}
	}
	return flag;
}

/*
reset everyone to not notified
input:  (void)
output: (void)
*/
void Game::reset()
{
	for (auto it = m_waiting.begin(); it != m_waiting.end(); ++it)
	{
		it->second = false;
	}
}

/*
nextQuestion everyone
input:  (void)
output: (void)
*/
void Game::nextQuestion()
{
	alreadyAskedQuestions.push_back(m_questionNum);
	if (alreadyAskedQuestions.size() == m_questions.size())
		while (alreadyAskedQuestions.size() >= 0)
			alreadyAskedQuestions.pop_back();
	while ((std::find(alreadyAskedQuestions.begin(), alreadyAskedQuestions.end(), m_questionNum) != alreadyAskedQuestions.end()))
	{
		srand(time(NULL));
		m_questionNum = rand() % m_questions.size();
	}

	for (auto it = m_players.begin(); it != m_players.end(); ++it)
	{
		(*it).second.currentQuestion = m_questions.at(m_questionNum % m_questions.size()).getStruct();
	}
}


/*
check if game is empty
input:  (void)
output: (bool)- if room is empty
*/
bool Game::checkPlayers()
{
	return (this->m_players.size() == 0 ? true : false);
}
