#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "LoggedUser.h"
struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

class Room
{
public:
	//Constructors
	Room() = default;
	Room(RoomData room);

	//Destructor
	~Room() = default;

	//Methods
	bool addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	void setActive(int active);

	//Getters
	std::vector<std::string> getAllUsers();
	std::string				 getRoomName();
	unsigned int			 getRoomId();
	unsigned int			 getRoomMaxUsers();
	unsigned int			 getRoomActivity();
	unsigned int			 getRoomQuestionCount();
	unsigned int			 getRoomTimePerQuestion();
	RoomData				 getRoomData();

private:
	//Properties
	RoomData			    m_metadata;
	std::vector<LoggedUser> m_users;
};
