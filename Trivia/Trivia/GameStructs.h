#pragma once
#include <string>
#include <vector>
#include <map>

typedef struct SubmitAnswerRequest
{
	float timer;
	unsigned int answerId;
}SubmitAnswerRequest;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct LeaveGameResponse
{
	unsigned int status;
}LeaveGameResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	std::string  question;
	std::map<unsigned int, std::string> answers;
}GetQuestionResponse;

typedef struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
}PlayerResults;

typedef struct GetGameResultResponse
{
	unsigned int status;
	std::vector<PlayerResults> results;
}GetGameResultResponse;

typedef struct QuestionStruct
{
	std::string question;
	std::string currAns;
	std::string ans2;
	std::string ans3;
	std::string ans4;
}QuestionStruct;

typedef struct Statistic
{
	int counter;
	bool isCurr;
	float time;
	std::string username;
}Statistic;

