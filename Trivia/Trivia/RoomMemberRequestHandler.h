#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include <iostream>

class RequestHandlerFactory;

class RoomMemberRequestHandler : IRequestHandler
{
public:
	//Constructor
	RoomMemberRequestHandler(RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user, Room* room);

	//Destructor
	~RoomMemberRequestHandler() = default;

	//Methods
	bool		  isRequestRelevant(RequestInfo) override;
	RequestResult handleRequest(RequestInfo)	 override;

private:
	//Properties
	Room*				   m_room;
	LoggedUser			   m_user;
	RoomManager&		   m_roomManager;
	RequestHandlerFactory& m_handlerFactory;

	//Methods
	RequestResult leaveRoom(RequestInfo req);
	RequestResult getRoomState(RequestInfo req);
	RequestResult changeHandler(RequestInfo req);
};
