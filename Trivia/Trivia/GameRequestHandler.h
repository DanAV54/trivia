#pragma once
#include "IRequestHandler.h"
#include "Game.h"
#include "GameManager.h"
#include "RequestHandlerFactory.h"
#include <iostream>
#include <mutex>
#include <thread>
#include <condition_variable>

class RequestHandlerFactory;

class GameRequestHandler : IRequestHandler
{
public:
	//Constructor
	GameRequestHandler(GameManager& gameMan, RequestHandlerFactory& reqHand, LoggedUser user, Game* game);

	//Destructor
	~GameRequestHandler() = default;

	//Methods
	bool		  isRequestRelevant(RequestInfo req) override;
	RequestResult handleRequest(RequestInfo req)	 override;

private:
	//Properties
	Game*				  				m_game;
	LoggedUser			   				m_user;
	GameManager&						m_gameManager;
	RequestHandlerFactory&				m_handlerFactory;
	std::map<unsigned int, std::string> m_answers;
	std::string							m_currAns;

	//Methods
	RequestResult getQuestion(RequestInfo req);
	RequestResult submitAnswer(RequestInfo req);
	RequestResult getGameResult(RequestInfo req);
	RequestResult leaveGame(RequestInfo req);
};
