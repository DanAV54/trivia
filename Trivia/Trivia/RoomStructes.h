#pragma once
#include "Room.h"
#include <string>
#include <vector>

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
}GetPlayersInRoomRequest;

typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
}GetPlayersInRoomResponse;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
}JoinRoomRequest;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeOut;
}CreateRoomRequest;

typedef struct CreateRoomResponse
{
	unsigned int status;
	unsigned int roomId;
}CreateRoomResponse;

typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
}GetRoomsResponse;

typedef struct GetHighScoreResponse
{
	unsigned int status;
	std::vector<std::string> statics;
}GetHighScoreResponse;

typedef struct GetPersonalStatsResponse
{
	unsigned int status;
	std::vector<std::string> statics;
}GetPersonalStatsResponse;

typedef struct AddQuestionRequest
{
	std::string question;
	std::string currAns;
	std::string ans2;
	std::string ans3;
	std::string ans4;
}AddQuestionRequest;

typedef struct AddQuestionResponse
{
	unsigned int status;
}AddQuestionResponse;


//VERSION 3
typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;
typedef struct StartGameResponse
{
	unsigned int status;
}StartGameResponse;
typedef struct GetRoomStateResponse
{
	unsigned int status;
	int hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	int answerTimeout;
}GetRoomStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
}LeaveRoomResponse;

//VERSION 3 HELPERS
typedef struct ChangeHandlerRequest
{
	int newHandler;
}ChangeHandlerRequest;

typedef struct ChangeHandlerResponse
{
	unsigned int status;
}ChangeHandlerResponse;
