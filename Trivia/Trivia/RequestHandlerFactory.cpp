#include "RequestHandlerFactory.h"

/*
Constructor
input: (IDatabase*) database- value to m_database
*/
RequestHandlerFactory::RequestHandlerFactory(IDatabase* dataBase): m_database(dataBase)
{
    this->m_loginManager = LoginManager(this->m_database);
    this->m_gameManager  = GameManager(this->m_database);
    m_StatisticsManager.setDataBase(m_database);
}

/*
Destructor
input: none
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
}

//CREATORS

/*
Creates Login Handler and return it
input:  none
output: (LoginRequestHandler*) [untitled]- LoginRequestHandler to return
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler() 
{
	return new LoginRequestHandler(std::ref(this->m_loginManager), std::ref(*this));
}

/*
Creates Menu Handler and return it
input:  (LoggedUser)          user- the user to handle
output: (MenuRequestHandler*) [untitled]- MenuRequestHandler to return
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
    return new MenuRequestHandler(std::ref(this->m_StatisticsManager), std::ref(this->m_roomManager), std::ref(*this), user);
}

/*
Creates Admin Handler and return it
input:  (LoggedUser)               user- the user to handle
        (Room*)                    room- the room to handle
output: (RoomAdminRequestHandler*) [untitled]- RoomAdminRequestHandler to return
*/
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room* room)
{
    return new RoomAdminRequestHandler(std::ref(this->m_roomManager), std::ref(*this), user, room);
}

/*
Creates Member Handler and return it
input:  (LoggedUser)                user- the user to handle
        (Room*)                     room- the room to handle
output: (RoomMemberRequestHandler*) [untitled]- RoomMemberRequestHandler to return
*/
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room* room)
{
    return new RoomMemberRequestHandler(std::ref(this->m_roomManager), std::ref(*this), user, room);
}

/*
Creates game Handler and return it
input:  (LoggedUser)          user- the user to handle
output: (GameRequestHandler*) [untitled]- GameRequestHandler to return
*/
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Game* game)
{
    return new GameRequestHandler(std::ref(this->m_gameManager), std::ref(*this), user, game);
}

//GETTERS

/*
Getter for m_loginManager
input:  none
output: (LoginManager&) m_loginManager- Getter
*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
    return m_loginManager;
}

/*
Getter for m_StatisticsManager
input:  none
output: (StatisticsManager&) m_StatisticsManager- Getter
*/
StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return m_StatisticsManager;
}

/*
Getter for m_roomManager
input:  none
output: (RoomManager&) m_roomManager- Getter
*/
RoomManager& RequestHandlerFactory::getRoomManager()
{
    return m_roomManager;
}

/*
Getter for m_gameManager
input:  none
output: (GameManager&) m_gameManager- Getter
*/
GameManager& RequestHandlerFactory::getGameManager()
{
    return m_gameManager;
}

