#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include <iostream>

class RequestHandlerFactory;

class RoomAdminRequestHandler : IRequestHandler
{
public:
	//Constructor
	RoomAdminRequestHandler(RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user, Room* room);

	//Destructor
	~RoomAdminRequestHandler() = default;

	//Methods
	bool		  isRequestRelevant(RequestInfo) override;
	RequestResult handleRequest(RequestInfo)	 override;

private:
	//Properties
	Room*				     m_room;
	LoggedUser			     m_user;
	RoomManager&		     m_roomManager;
	RequestHandlerFactory&   m_handlerFactory;

	//Methods
	RequestResult closeRoom(RequestInfo);
	RequestResult startGame(RequestInfo);
	RequestResult getRoomState(RequestInfo);
};
