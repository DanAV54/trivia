#pragma once
#include <string>
#include <list>
#include "LoginStructs.h"
#include "GameStructs.h"

class IDatabase
{
public:
	//Constructor
	IDatabase();

	//Destructor
	virtual ~IDatabase();

	//Methods
	virtual std::list<Statistic>	  getStaticsOfUser(std::string username)			= 0;
	virtual std::list<QuestionStruct> getQuestions()									= 0;
	virtual std::list<SignupRequest>  getUsers()								 		= 0;
	virtual bool					  doesUserExist(std::string)						= 0;
	virtual bool 					  doesPasswordMatch(std::string, std::string)	    = 0;
	virtual bool					  addNewUser(std::string, std::string, std::string) = 0;
	virtual float 					  getPlayerAverageAnswerTime(std::string)		    = 0;
	virtual int 					  getNumOfCorrectAnswers(std::string)			    = 0;
	virtual int 					  getNumOfTotalAnswers(std::string)				    = 0;
	virtual int 					  getNumOfPlayerGames(std::string)				    = 0;
	virtual void					  addUserData(Statistic stats)					    = 0;
	virtual void					  addQuestion(std::string question, std::string curr_ans, std::string ans2, std::string ans3, std::string ans4) = 0;
	virtual void					  startGame(std::string username)                   = 0;

};
