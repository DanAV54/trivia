#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include <iostream>
#include <map>

//OTHER DEFINES
#define POSITIVE_STATUS         1
#define NEGATIVE_STATUS         0
#define SPLIT_CHAR				","

class RequestHandlerFactory;

class MenuRequestHandler : IRequestHandler
{
public:
	//Constructors
	MenuRequestHandler(StatisticsManager& statMan, RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user);

	//Methods
	bool		  isRequestRelevant(RequestInfo req) override;
	RequestResult handleRequest(RequestInfo req)     override;

private:
	//Properties
	RequestHandlerFactory& m_handlerFactory;
	StatisticsManager&	   m_staticsManager;
	RoomManager&		   m_roomManager;
	LoggedUser			   m_loggedUser;

	//Methods
	RequestResult logout(RequestInfo req);
	RequestResult getRooms(RequestInfo req);
	RequestResult getPlayersInRoom(RequestInfo req);
	RequestResult getPersonalStats(RequestInfo req);
	RequestResult getHighScore(RequestInfo req);
	RequestResult joinRoom(RequestInfo req);
	RequestResult createRoom(RequestInfo req);
	RequestResult addQuestion(RequestInfo req);
};