#include "GameRequestHandler.h"

std::mutex mtx;
std::condition_variable cv;
std::unique_lock<std::mutex> lk(mtx);

/*
Constructor
input: (GameManager&)		    roomMan- value to gameManager
	   (RequestHandlerFactory&) reqHand- value to RequestHandlerFactory
	   (LoggedUser)				user- value to loggedUser
	   (Game*)					room- value to game
*/
GameRequestHandler::GameRequestHandler(GameManager& gameMan, RequestHandlerFactory& reqHand, LoggedUser user, Game* game): m_gameManager(gameMan), m_handlerFactory(reqHand), m_user(user), m_game(game)
{
}

/*
Checks if The Request is Relevent- Relevant Request is a req that matches the state the client is in now (Menu and so on)
input:  (RequestInfo) req- the request info keep the id inside and if the id matches room stuff so true
output: (Bool)        true/false- relevent/not relevent
*/
bool GameRequestHandler::isRequestRelevant(RequestInfo req)
{
	return (req.id == GET_GAME_RESULT ||
			req.id == SUBMIT_ANSWER	  ||
			req.id == GET_QUESTION	  ||
			req.id == LEAVE_GAME);
}

/*
Handle Request, and bridging between Code to Func, and return their result
input:  (RequestInfo)   req- the data of the request type
output: (RequestResult) reqRes- the result from login/signup
*/
RequestResult GameRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;

	if (req.id == GET_GAME_RESULT)
	{
		reqRes = this->getGameResult(req);
	}
	else if (req.id == SUBMIT_ANSWER)
	{
		reqRes = this->submitAnswer(req);
	}
	else if (req.id == GET_QUESTION)
	{
		reqRes = this->getQuestion(req);
	}
	else if (req.id == LEAVE_GAME)
	{
		reqRes = this->leaveGame(req);
	}
	return reqRes;
}

/*
get next question, if it succeeded so send success and question
input:  (RequestInfo)   req- the data of the getQuestion request
output: (RequestResult) reqRes- the result from getQuestion
*/
RequestResult GameRequestHandler::getQuestion(RequestInfo req)
{
	RequestResult       reqRes;
	GetQuestionResponse getQuestRes;


	if (this->m_game->notify(this->m_user))
	{
		m_game->nextQuestion();
		cv.notify_all();
		m_game->reset();
	}
	else
	{
		cv.wait(lk);
	}

	getQuestRes.question   = this->m_game->getQuestionForUser(this->m_user).question;
	srand(time(NULL));
	int randNum = rand() % (4);
	
	getQuestRes.answers[randNum + 1] = this->m_game->getQuestionForUser(this->m_user).currAns;
	getQuestRes.answers[((randNum + 1) %4) + 1] = this->m_game->getQuestionForUser(this->m_user).ans2;
	getQuestRes.answers[((randNum + 2) % 4) + 1] = this->m_game->getQuestionForUser(this->m_user).ans3;
	getQuestRes.answers[((randNum + 3) % 4) + 1] = this->m_game->getQuestionForUser(this->m_user).ans4;
	m_answers = getQuestRes.answers;
	m_answers[0] = "";
	m_currAns = this->m_game->getQuestionForUser(this->m_user).currAns;

	getQuestRes.status = POSITIVE_STATUS;
	reqRes.response    = JsonResponsePacketSerializer::serializeResponse(getQuestRes);
	reqRes.newHandler  = nullptr;
	return reqRes;
}

/*
submit answer, if it succeeded so send success
input:  (RequestInfo)   req- the data of the submitAnswer request
output: (RequestResult) reqRes- the result from submitAnswer
*/
RequestResult GameRequestHandler::submitAnswer(RequestInfo req)
{
	RequestResult        reqRes;
	SubmitAnswerResponse submitAnsRes;
	SubmitAnswerRequest  submitAnsReq;

	submitAnsReq = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(req.buffer);

	m_game->submitAnswer(m_user, m_answers[submitAnsReq.answerId], submitAnsReq.timer);

	submitAnsRes.status = POSITIVE_STATUS;
	for (auto& it : m_answers) 
	{
		if (it.second == m_currAns)
		{
			submitAnsRes.correctAnswerId = it.first;
		}
	}
	reqRes.response     = JsonResponsePacketSerializer::serializeResponse(submitAnsRes);
	reqRes.newHandler   = nullptr;
	return reqRes;
}

/*
get the game result, if it succeeded so send success and results
input:  (RequestInfo)   req- the data of the getGameResult request
output: (RequestResult) reqRes- the result from getGameResult
*/
RequestResult GameRequestHandler::getGameResult(RequestInfo req)
{
	RequestResult         reqRes;
	GetGameResultResponse getGameResultRes;

	getGameResultRes.results = this->m_gameManager.getResults(m_game);
	
	getGameResultRes.status = POSITIVE_STATUS;
	reqRes.response         = JsonResponsePacketSerializer::serializeResponse(getGameResultRes);
	reqRes.newHandler	    = nullptr;
	return reqRes;
}

/*
leave the game, if it succeeded so send success
input:  (RequestInfo)   req- the data of the leaveGame request
output: (RequestResult) reqRes- the result from leaveGame
*/
RequestResult GameRequestHandler::leaveGame(RequestInfo req)
{
	RequestResult     reqRes;
	LeaveGameResponse leaveGameRes;
	int				  temp;

	this->m_game->removePlayer(this->m_user);
	this->m_handlerFactory.getRoomManager().getRoom(m_game->getGameId())->removeUser(m_user);
	if (this->m_game->checkPlayers())
	{
		temp = m_game->getGameId();
		this->m_gameManager.deleteGame(this->m_game);
		this->m_handlerFactory.getRoomManager().deleteRoom(temp);
	}

	leaveGameRes.status = POSITIVE_STATUS;
	reqRes.response     = JsonResponsePacketSerializer::serializeResponse(leaveGameRes);
	reqRes.newHandler   = (IRequestHandler*)this->m_handlerFactory.createMenuRequestHandler(m_user);
	return reqRes;
}
