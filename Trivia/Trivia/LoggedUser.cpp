#include "LoggedUser.h"

/*
Constructor
input: none
*/
LoggedUser::LoggedUser()
{
}

/*
Constructor
input: (String) username- value to m_username
*/
LoggedUser::LoggedUser(std::string username)
{
    this->m_username = username;
}

/*
Destructor
input: none
*/
LoggedUser::~LoggedUser()
{
}

/*
Setter For m_username
input:  (std::string) name- value to set with
output: (void)
*/
void LoggedUser::setUsername(std::string name)
{
    m_username = name;
}

/*
Operator < for map
input:  (LoggedUser&) other- user to compare to
output: (bool)        [untitled] if this < from other
*/
bool LoggedUser::operator<(const LoggedUser& other) const
{
    return this->m_username < other.m_username;
}

/*
Operator > for map
input:  (LoggedUser&) other- user to compare to
output: (bool)        [untitled] if this > from other
*/
bool LoggedUser::operator>(const LoggedUser& other) const
{
    return this->m_username > other.m_username;
}

/*
Getter For m_username
input:  (void)
output: (String) m_username- getter
*/
std::string LoggedUser::getUsername() const
{
    return m_username;
}
