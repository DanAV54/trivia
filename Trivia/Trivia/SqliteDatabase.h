#pragma once
#include "IDatabase.h"
#include <io.h>
#include "sqlite3.h"
#include <list>
#include "LoginStructs.h"
#include "GameStructs.h"

class SqliteDatabase : public IDatabase
{
public:
	//Constructor
	SqliteDatabase();

	//Destructor
	~SqliteDatabase() = default;

	//Methods
	void addQuestion(std::string question, std::string curr_ans, std::string ans2, std::string ans3, std::string ans4) override;
	void addUserData(Statistic stats)											   override;
	bool doesUserExist(std::string username)									   override;
	bool doesPasswordMatch(std::string username, std::string password)			   override;
	bool addNewUser(std::string username, std::string password, std::string email) override;

	//Getters
	std::list<QuestionStruct> getQuestions()									  override;
	std::list<SignupRequest>  getUsers()										  override;
	float					  getPlayerAverageAnswerTime(std::string username)    override;
	int						  getNumOfCorrectAnswers(std::string username)	      override;
	int						  getNumOfTotalAnswers(std::string username)	  	  override;
	int						  getNumOfPlayerGames(std::string username)		      override;
	std::list<Statistic>	  getStaticsOfUser(std::string username)			  override;
	void					  startGame(std::string username)                     override;

private:
	//Properties
	std::list<SignupRequest>  m_users;
	std::list<QuestionStruct> m_questions;
	std::list<Statistic>	  m_statistic;
	sqlite3*				  m_db;

	//Methods
	static void send(sqlite3* db, std::string message, int callback(void*, int, char**, char**) = nullptr, void* data = nullptr);
	static int  usersCallback(void* data, int argc, char** argv, char** azColName);
	static int  questionsCallback(void* data, int argc, char** argv, char** azColName);
	static int  statisticsCallback(void* data, int argc, char** argv, char** azColName);	
};
