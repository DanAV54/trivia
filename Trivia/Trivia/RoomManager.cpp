#include "RoomManager.h"

/*
creates room
input:  (LoggedUser) user- the admin
		(RoomData)   roomData- the data of the room
output: (Room*)		 room- the room
*/
Room* RoomManager::createRoom(LoggedUser user, RoomData roomData)
{
	unsigned int roomId = 0;
	roomId              = this->m_rooms.size(); //get the lowest room Id that isnt taken
	roomData.id         = roomId;
	roomData.isActive   = 1;
	Room* room          = new Room(roomData);

	(*room).addUser(user);
	m_rooms.insert(std::pair<unsigned int, Room*>(roomId, room));
	return room;
}

/*
delete room
input:  (int)  ID- the id of the room
output: (void)
*/
void RoomManager::deleteRoom(unsigned int ID)
{
	std::cout << ID << std::endl;
	if (!m_rooms.empty())
	{
		for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
		{
			if ((int)it->first == ID)
			{
				delete (it->second);
				m_rooms.erase(it);
				break;
			}
		}
	}

}

/*
return the room state - [0-IsntExist, 1-WaitToStart, 2-Active] 
input:  (int)		   ID- the id of the room
output: (unsigned int) [the activity]- the activity
*/
unsigned int RoomManager::getRoomState(int ID)
{
	if (!m_rooms.empty())
	{
		for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
		{
			if ((int)it->first == ID)
			{
				return (*(it->second)).getRoomActivity();
			}
		}
	}
	return 0; //can't find the room
}

/*
Getter for rooms
input:  (void)
output: (std::vector<RoomData>) roomsVec- the rooms
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomsVec;
	if (!m_rooms.empty())
	{
		for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
			roomsVec.push_back((*(it->second)).getRoomData());
	}
	return roomsVec;
}

/*
Getter for specific room
input:  (unsigned int) id- the id of the room
output: (Room*)		   [room]- the room
*/
Room* RoomManager::getRoom(unsigned int id)
{
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		if (it->first == id)
			return (it->second);
	}
	throw(std::exception("ERROR: this id isnt exsist!"));
	Room* empty;
	return empty;
}

