#pragma once
#include <vector>
#include "Room.h"
#include <map>

class RoomManager
{
public:
	//Constructor
	RoomManager() = default;

	//Destructor
	~RoomManager() = default;

	//Methods
	Room* createRoom(LoggedUser user, RoomData roomData);
	void  deleteRoom(unsigned int ID);

	//Getters
	unsigned int		  getRoomState(int ID);
	std::vector<RoomData> getRooms();
	Room*				  getRoom(unsigned int id);

private:
	//Properties
	std::map<unsigned int, Room*> m_rooms;
};