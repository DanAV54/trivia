#pragma once
#include "GameStructs.h"
#include <string>
#include <vector>

class Question
{
public:
	//Constructors
	Question() = default;
	Question(QuestionStruct q);

	//Destructors
	~Question() = default;

	//Getters
	std::vector<std::string> getPossibleAnswers();
	std::string				 getQuestion();
	std::string				 getCorrentAnswer();
	QuestionStruct			 getStruct();

private:
	//Properties
	std::vector<std::string> m_possibleAnswers;
	std::string				 m_question;
	std::string				 m_currAns;
};
