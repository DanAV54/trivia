#pragma once
#include <string>
#include <vector>

typedef struct LoginRequest
{
	std::string username;
	std::string password;
}LoginRequest;

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
	int games;
}SignupRequest;

typedef struct SignupResponse
{
	int status;
}SignupResponse;

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;



