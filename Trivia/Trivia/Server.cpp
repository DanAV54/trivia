#include "Server.h"

/*
Constructor- Set all Properties
input: none
*/
Server::Server() : m_database(nullptr), m_handlerFactory(NULL), m_communicator(this->m_handlerFactory)
{
	this->m_database = new SqliteDatabase();
	this->m_handlerFactory = RequestHandlerFactory(this->m_database);
	this->m_communicator.setHandlerFactory(this->m_handlerFactory);
}

/*
Destructor
input: none
*/
Server::~Server()
{
}

/*
Runs the whole System
input:  none
output: none
*/
void Server::run()
{
	std::string text;
	try
	{
		WSAInitializer WSAInitializer;
		std::thread comThread(&Communicator::startHandleRequests, this->m_communicator);
		comThread.detach();
		while (text != "EXIT")
		{
			std::cin >> text;
		}
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
}