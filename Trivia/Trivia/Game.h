#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "LoggedUser.h"
#include "Question.h"
#include <map>
#include <time.h>
#include "GameStructs.h"
#include "IDatabase.h"
struct GameData
{
	QuestionStruct currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;
};


class Game
{
public:
	//Constructors
	Game() = default;
	Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players, IDatabase* DB, int gameId);

	//Destructor
	~Game() = default;

	//Methods
	void submitAnswer(LoggedUser user, std::string answer, float time);
	void removePlayer(LoggedUser user);
	bool notify(LoggedUser);
	void reset();
	bool checkPlayers();
	void nextQuestion();

	//Getters
	std::map<LoggedUser, GameData> getPlayers();
	QuestionStruct				   getQuestionForUser(LoggedUser user);
	int							   getGameId();

	//Operators
	bool operator ==(const Game& other) const;

private:
	//Properties
	std::map<LoggedUser, GameData> m_players;
	std::map<LoggedUser, bool>     m_waiting;
	std::vector<Question>          m_questions;
	IDatabase*					   m_dataBase;
	int 						   m_questionNum;
	int 						   m_gameId;
	std::list<int>				   alreadyAskedQuestions;
};
