#pragma once
#pragma comment(lib, "ws2_32.lib")

#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <iostream>
#include <thread>
#include <string>
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonResponsePacketSerializer.h"

#define ERROR_SENDING_MSG  "Error while sending message to client"
#define ERROR_RECEIVE_MSG  "Error while recieving from socket: "
#define CONNECTION_MSG     "Waiting for client connection request"
#define ERROR_RESPONSE_MSG "ERROR"
#define ID_BYTES           2          
#define MSG_SIZE_BYTES     4          

#define PORT 9090

class Communicator
{
public:
	//Constructor
	Communicator(RequestHandlerFactory& reqHand);

	//Distructor
	~Communicator();

	//Methods
	void startHandleRequests();

	//Setters
	void setHandlerFactory(RequestHandlerFactory& reqHand);

private:
	//Properties
	SOCKET                             m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory&             m_handlerFactory;

	//Methods
	void acceptClient();
	void bindAndListen(int port);
	void handleNewClient(SOCKET sc);

	//Helpers
	std::string recFromUser(SOCKET clientSocket, int size);
	RequestInfo createRequestInfo(SOCKET clientSocket);
	void        sendToUser(SOCKET clientSocket, char* sender);
	void        relevantReq(SOCKET clientSocket, RequestInfo reqInf);
	void        unrelevantReq(SOCKET clientSocket);
};
