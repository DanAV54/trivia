#pragma once
#include <vector>
#include <iostream>
#include "IDatabase.h"
#include "LoggedUser.h"
#include "LoginStructs.h"
#include "IDatabase.h"

class LoginManager
{
public:
	//Constructors
	LoginManager() = default;
	LoginManager(IDatabase* datebase);
	
	//Destructors
	~LoginManager();

	//Methods
	bool signup(std::string username, std::string password, std::string email);
	bool login(std::string username, std::string password);
	bool logout(std::string username);

	//Setters
	void setDatabase(IDatabase* datebase);

private:
	//Properties
	IDatabase*              m_database;
	std::vector<LoggedUser>	m_loggedUsers; 
};
