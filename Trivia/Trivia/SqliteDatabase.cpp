#include "SqliteDatabase.h"
#include <iostream>

/*
Constructor- Creates the tables and file if neccacery
input: none
*/
SqliteDatabase::SqliteDatabase()
{
	sqlite3*    db;
	std::string dbFileName = "triviaDB.sqlite";
	std::string sqlStatement;

	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res           = sqlite3_open(dbFileName.c_str(), &db);
	this->m_db        = db;

	if (res != SQLITE_OK)
	{
		this->m_db = nullptr;
		throw(std::exception("Failed to open DB"));
	}
	if (doesFileExist == -1) {
		// USERS
		sqlStatement = "CREATE TABLE USERS (USERNAME TEXT PRIMARY KEY NOT NULL, PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL, GAMES INTEGER NOT NULL); ";
		send(db, sqlStatement);
		// Questions
		sqlStatement = "CREATE TABLE QUESTIONS (QUESTION TEXT PRIMARY KEY NOT NULL, CURR_ANS TEXT NOT NULL, ANS2 TEXT NOT NULL, ANS3 TEXT NOT NULL, ANS4 TEXT NOT NULL); ";
		send(db, sqlStatement);
		// Statistics
		sqlStatement = "CREATE TABLE STATISTICS (COUNTER INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, IS_CURR INTEGER NOT NULL, TIME REAL NOT NULL, USERNAME TEXT NOT NULL, Foreign KEY (USERNAME) REFERENCES USERS(USERNAME));";
		send(db, sqlStatement);

	}
	else
	{
		send(m_db, "SELECT * FROM USERS", usersCallback, (void*)&m_users);
		send(m_db, "SELECT * FROM QUESTIONS", questionsCallback, (void*)&m_questions);
		send(m_db, "SELECT * FROM STATISTICS", statisticsCallback, (void*)&m_statistic);
	}
	if (m_users.empty())
		std::cout << "No Uers\n";
}

/*
Checks if User Exist
input:  (String) username- name to check by
output: (bool)   true/false- if user exists
*/
bool SqliteDatabase::doesUserExist(std::string username)
{
	for (const auto& user : m_users) {
		if (user.username == username) {
			return true;
		}
	}
	return false;
}

/*
Check if The username Matches the Password
input:  (String) username- name to check by
	    (String) password- pass to check by
output: (bool)   true/false- if they match
*/
bool SqliteDatabase::doesPasswordMatch(std::string username, std::string password)
{
	for (const auto& user : m_users) {
		if (user.username == username) {
			return user.password == password;
		}
	}
	throw(std::exception("Error! user doesn't exist"));
}

/*
Insert New User to the Database
input:  (String) username- name to insert
		(String) password- pass to insert
		(String) email- mail to insert
output: (bool)   true/false- if it added succesfully
*/
bool SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{
	if (doesUserExist(username))
		return false;
	m_users.push_back(SignupRequest{ username, password, email , 0 });
	std::string massage = "INSERT INTO USERS VALUES ('" + username + "', '" + password + "', '" + email + "', 0);";
	send(m_db, massage);
	return true;
}

/*
Getter to questions
input:  (void)
output: (std::list<Question>) m_questions
*/
std::list<QuestionStruct> SqliteDatabase::getQuestions()
{
	return m_questions;
}

/*
Getter to users
input:  (void)
output: (std::list<SignupRequest>) m_users
*/
std::list<SignupRequest> SqliteDatabase::getUsers()
{
	return m_users;
}

/*
Getter to player's Average Time
input:  (std::string) username- the player to get the average for
output: (float)       sumTime / numOfQuestions- result;
*/
float SqliteDatabase::getPlayerAverageAnswerTime(std::string username)
{
	float sumTime = 0.0;
	int numOfQuestions = 0;
	for (auto it = m_statistic.begin(); it != m_statistic.end(); it++)
	{
		if (it->username == username)
		{
			sumTime += it->time;
			numOfQuestions++;
		}
	}
	if (numOfQuestions == 0) // cant devide by 0
		return 0;
	return sumTime / numOfQuestions;
}

/*
Getter to player's correct ansers
input:  (std::string) username- the player to get the correctAnsers for
output: (int)         currAnswer- result;
*/
int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
	int currAnswer = 0;
	for (auto it = m_statistic.begin(); it != m_statistic.end(); it++)
	{
		if (it->username == username && it->isCurr)
			currAnswer++;
	}
	return currAnswer;
}

/*
Getter to player's total ansers
input:  (std::string) username- the player to get the TotalAnswers for
output: (int)         answers- result;
*/
int SqliteDatabase::getNumOfTotalAnswers(std::string username)
{
	int answers = 0;
	for (auto it = m_statistic.begin(); it != m_statistic.end(); it++)
	{
		if (it->username == username) 
			answers++;
	}
	return answers;
}

//???
int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
	for (auto it = m_users.begin(); it != m_users.end(); it++)
		if (it->username == username)
			return it->games;
	return 0;
}

/*
Getter to player's statics
input:  (std::string)		   username- the player to get the statics for
output: (std::list<Statistic>) statics- result;
*/
std::list<Statistic> SqliteDatabase::getStaticsOfUser(std::string username)
{
	std::list<Statistic> statics;
	for (auto it = m_statistic.begin(); it != m_statistic.end(); it++)
	{
		if (it->username == username)
			statics.push_back(*it);
	}
	return statics;
}

void SqliteDatabase::startGame(std::string username)
{
	for (auto it = m_users.begin(); it != m_users.end(); it++)
	{
		if (it->username == username)
		{
			it->games++;
			send(m_db, "UPDATE USERS SET GAMES = " + std::to_string(it->games) + " WHERE USERNAME == '" + username + "';");
		}
	}
}

/*
add user statistics
input:  (Statistic) statics- statics to add
output: (void)
*/
void SqliteDatabase::addUserData(Statistic stats)
{
	m_statistic.push_back(stats);
	send(m_db, "INSERT INTO STATISTICS(IS_CURR, TIME, USERNAME) VALUES(" + std::to_string(stats.isCurr ? 1 : 0)  + ", " + std::to_string(stats.time) + ", '" + stats.username + "'); ");
}

/*
Helper To add Questions to DB
input:  (std::string) question- the question
		(std::string) curr_ans- correct answer
		(std::string) ans2
		(std::string) ans3
		(std::string) ans4
output: (void)
*/
void SqliteDatabase::addQuestion(std::string question, std::string curr_ans, std::string ans2, std::string ans3, std::string ans4)
{
	std::string massage = "INSERT INTO QUESTIONS VALUES ('" + question + "', '" + curr_ans + "', '" + ans2 + "', '" + ans3 + "', '" + ans4 + "');";
	send(m_db, massage);
	QuestionStruct que;
	que.question = question;
	que.currAns = curr_ans;
	que.ans2 = ans2;
	que.ans3 = ans3;
	que.ans4 = ans4;
	m_questions.push_back(que);
}

/*
Sends SQL Requests
input:  (sqlite3*)         db- the database to sendto
		(String)           message- the message to send
		(Integer Function) callback- callback function to the database
		(Void*)            data- data for the callback if neccery
output: none
*/
void SqliteDatabase::send(sqlite3* db, std::string message, int callback(void*, int, char**, char**), void* data)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, message.c_str(), callback, data, &errMessage);
	if (res != SQLITE_OK)
		std::cerr << errMessage << std::endl;
}

/*
Get the users from DB to the list
input:  [Callback Arguments]
output: (Integer)          0- success
*/
int SqliteDatabase::usersCallback(void* data, int argc, char** argv, char** azColName)
{
	std::string username = "";
	std::string password = "";
	std::string email = "";
	int games = -1;
	for (int i = 0; i < argc; i++)
	{
		if (!strcmp(azColName[i], "USERNAME"))
		{
			username = argv[i];
		}
		else if (!strcmp(azColName[i], "PASSWORD"))
		{
			password = argv[i];
		}
		else if (!strcmp(azColName[i], "EMAIL"))
		{
			email = argv[i];
		}
		else if (!strcmp(azColName[i], "GAMES"))
		{
			games = atoi(argv[i]);
		}
		else
		{
			throw(std::exception("Error! wrong input from DB"));
		}
		if (username != "" && password != "" && email != "" && games != -1)
		{
			((std::list<SignupRequest>*)data)->push_back(SignupRequest{ username, password, email, games });
			username = "";
			password = "";
			email = "";
			games = -1;
		}
	}

	return 0;
}

/*
Get the question from DB to the list
input:  [Callback Arguments]
output: (Integer)          0- success
*/
int SqliteDatabase::questionsCallback(void* data, int argc, char** argv, char** azColName)
{
	std::string question = "";
	std::string curr_ans = "";
	std::string ans2 = "";
	std::string ans3 = "";
	std::string ans4 = "";
	for (int i = 0; i < argc; i++)
	{
		if (!strcmp(azColName[i], "QUESTION"))
		{
			question = argv[i];
		}
		else if (!strcmp(azColName[i], "CURR_ANS"))
		{
			curr_ans = argv[i];
		}
		else if (!strcmp(azColName[i], "ANS2"))
		{
			ans2 = argv[i];
		}
		else if (!strcmp(azColName[i], "ANS3"))
		{
			ans3 = argv[i];
		}
		else if (!strcmp(azColName[i], "ANS4"))
		{
			ans4 = argv[i];
		}
		else
		{
			throw(std::exception("Error! wrong input from DB"));
		}
		if ( question != "" &&  curr_ans != "" &&  ans2 != "" && ans3 != "" && ans4 != "")
		{
			((std::list<QuestionStruct>*)data)->push_back(QuestionStruct{ question, curr_ans, ans2, ans3, ans4 });
			question = "";
			curr_ans = "";
			ans2 = "";
			ans3 = "";
			ans4 = "";
		}
	}

	return 0;
}

/*
Get the statistics from DB to the list
input:  [Callback Arguments]
output: (Integer)          0- success
*/
int SqliteDatabase::statisticsCallback(void* data, int argc, char** argv, char** azColName)
{

	int counter = -1;
	int isCurr = -1;
	float time = -1;
	std::string username = "";
	for (int i = 0; i < argc; i++)
	{
		if (!strcmp(azColName[i], "COUNTER"))
		{
			counter = atoi(argv[i]);
		}
		else if (!strcmp(azColName[i], "IS_CURR"))
		{
			isCurr = atoi(argv[i]);
		}
		else if (!strcmp(azColName[i], "TIME"))
		{
			time = std::stof(argv[i]);
		}
		else if (!strcmp(azColName[i], "USERNAME"))
		{
			username = argv[i];
		}
		else
		{
			throw(std::exception("Error! wrong input from DB"));
		}
		if (counter != -1 && isCurr != -1 && time != -1 && username != "")
		{
			((std::list<Statistic>*)data)->push_back(Statistic{ counter, isCurr == 1, time, username});
			counter = -1;
			isCurr = -1;
			time = -1;
			username = "";
		}
	}

	return 0;
}
