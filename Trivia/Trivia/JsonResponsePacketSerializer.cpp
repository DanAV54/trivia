#include "JsonResponsePacketSerializer.h"
#include "json.hpp"

//VERSION 1

/*
Serialize (From JSON to buffer) Error Response
input:  (ErrorResponse)        eResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse eResp)
{
	nlohmann::json j;
	j["message"] = eResp.message;

	std::vector<unsigned char> buffer = jsonToBuffer(ERROR_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) Login Response
input:  (LoginResponse)        lResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse lResp)
{
	nlohmann::json j;
	j["status"] = lResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(LOGIN_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) Signup Response
input:  (SignupResponse)       sResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse sResp)
{
	nlohmann::json j;
	j["status"] = sResp.status;
	
	std::vector<unsigned char> buffer = jsonToBuffer(SIGNUP_CODE, j.dump().length(), j.dump());
	return buffer;
}

//VERSION 2

/*
Serialize (From JSON to buffer) Logout Response
input:  (LogoutResponse)       lResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse lResp)
{
	nlohmann::json j;
	j["status"] = lResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(LOGOUT_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) getRoom Response
input:  (GetRoomsResponse)     gResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse gResp)
{
	std::string    playersStr = "[";
	nlohmann::json j;

	j["status"] = gResp.status;

	for (auto it = gResp.rooms.begin(); it != gResp.rooms.end(); ++it)
	{
		playersStr += "{'name':'" + (*it).name + "','id':" + std::to_string((*it).id) + ",'maxPlayers':" + std::to_string((*it).maxPlayers) + ",'numOfQuestionInGame':" + 
			std::to_string((*it).numOfQuestionsInGame) + ",'timePerQuestion':" + std::to_string((*it).timePerQuestion) + +",'isActive':" + std::to_string((*it).isActive) + "},";
	}
	if (!gResp.rooms.empty())
	{
		playersStr = playersStr.substr(0, playersStr.size() - 1);
	}
	playersStr += "]";
	j["Rooms"] = playersStr;

	std::vector<unsigned char> buffer = jsonToBuffer(GET_ROOM, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) getPlayersInRoom Response
input:  (GetPlayersInRoomResponse) gResp - The Response to serialize
output: (unsigned char vector)     buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse gResp)
{
	nlohmann::json j;
	std::string    playersStr;

	for (auto it = gResp.players.begin(); it != gResp.players.end(); ++it)
	{
		playersStr += *it + ", ";
	}
	j["PlayersInRoom"] = playersStr.substr(0, playersStr.size() - 2);
	
	std::vector<unsigned char> buffer = jsonToBuffer(GET_PLAYERS_IN_ROOM, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) joinRoom Response
input:  (JoinRoomResponse)     jResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse jResp)
{
	nlohmann::json j;
	j["status"] = jResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(JOIN_ROOM, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) createRoom Response
input:  (CreateRoomResponse)   cResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse cResp)
{
	nlohmann::json j;
	j["status"] = cResp.status;
	j["roomId"] = cResp.roomId;

	std::vector<unsigned char> buffer = jsonToBuffer(CREATE_ROOM, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) GetHighScore Response
input:  (GetHighScoreResponse) gResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse gResp)
{
	std::string	   playersStr = "[";
	nlohmann::json j;

	j["status"] = gResp.status;

	for (auto it = gResp.statics.begin(); it != gResp.statics.end(); ++it)
	{
		playersStr += "'" + (*it) + "',";
	}
	playersStr = playersStr.substr(0, playersStr.size() - 1);
	playersStr += "]";
	j["statics"] = playersStr;

	std::vector<unsigned char> buffer = jsonToBuffer(GET_HIGH_SCORE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) GetPersonalStats Response
input:  (GetPersonalStatsResponse) gResp - The Response to serialize
output: (unsigned char vector)	   buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse gResp)
{
	nlohmann::json j;
	std::string	   playersStr = "[";
	
	j["status"] = gResp.status;
	
	for (auto it = gResp.statics.begin(); it != gResp.statics.end(); ++it)
	{
		playersStr += (*it) + ",";
	}
	playersStr = playersStr.substr(0, playersStr.size() - 1);
	playersStr += "]";
	j["statics"] = playersStr;

	std::vector<unsigned char> buffer = jsonToBuffer(GET_PERSONAL_STATS, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) AddQuestionResponse Response
input:  (AddQuestionResponse)  aResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(AddQuestionResponse aResp)
{
	nlohmann::json j;
	j["status"] = aResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(ADD_QUESTION, j.dump().length(), j.dump());
	return buffer;
}

//VERSION 3

/*
Serialize (From JSON to buffer) CloseRoomResponse Response
input:  (CloseRoomResponse)    cResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse cResp)
{
	nlohmann::json j;
	j["status"] = cResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(CLOSE_ROOM_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) StartGameResponse Response
input:  (StartGameResponse)    sResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse sResp)
{
	nlohmann::json j;
	j["status"] = sResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(START_GAME_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) GetRoomStateResponse Response
input:  (GetRoomStateResponse) gResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse gResp)
{
	nlohmann::json j;
	std::string    playersStr;

	j["status"]		  = gResp.status;
	j["hasGameBegun"] = gResp.hasGameBegun;
	for (auto it = gResp.players.begin(); it != gResp.players.end(); ++it)
	{
		playersStr += *it + ", ";
	}
	j["players"]	   = playersStr.substr(0, playersStr.length() -2);
	j["questionCount"] = gResp.questionCount;
	j["answerTimeout"] = gResp.answerTimeout;

	std::vector<unsigned char> buffer = jsonToBuffer(GET_STATE_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) LeaveRoomResponse Response
input:  (LeaveRoomResponse)	   lResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse lResp)
{
	nlohmann::json j;
	j["status"] = lResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(LEAVE_ROOM_CODE, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) ChangeHandlerResponse Response
input:  (ChangeHandlerResponse)	cResp - The Response to serialize
output: (unsigned char vector)  buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ChangeHandlerResponse cResp)
{
	nlohmann::json j;
	j["status"] = cResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(CHANGE_HANDLER, j.dump().length(), j.dump());
	return buffer;
}

//VERSION 4

/*
Serialize (From JSON to buffer) GetGameResultResponse Response
input:  (GetGameResultResponse)	gResp - The Response to serialize
output: (unsigned char vector)  buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultResponse gResp)
{
	nlohmann::json j;
	std::string	   playersStr ="[";

	j["status"] = gResp.status;
	for (auto it = gResp.results.begin(); it != gResp.results.end(); ++it)
	{
		playersStr += "{'username':'" + (*it).username + "','corrAns':" + std::to_string((*it).correctAnswerCount) + ",'wrongAns':" + std::to_string((*it).wrongAnswerCount) + ",'avg':" + std::to_string((*it).averageAnswerTime) + "},";
	}
	playersStr = playersStr.substr(0, playersStr.size() - 1);
	playersStr += "]";
	j["results"] = playersStr;
	
	std::vector<unsigned char> buffer = jsonToBuffer(GET_GAME_RESULT, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) SubmitAnswerResponse Response
input:  (SubmitAnswerResponse) sResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse sResp)
{
	nlohmann::json j;
	j["status"]          = sResp.status;
	j["correctAnswerId"] = sResp.correctAnswerId;

	std::vector<unsigned char> buffer = jsonToBuffer(SUBMIT_ANSWER, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) GetQuestionResponse Response
input:  (GetQuestionResponse)  gResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse gResp)
{
	nlohmann::json j;
	j["status"]     = gResp.status;
	j["question"]   = gResp.question;
	std::string msg = "{1:'" + gResp.answers[1] + "',2: '" + gResp.answers[2] + "',3: '" + gResp.answers[3] + "',4: '" + gResp.answers[4] + "'}";
	j["answers"]    = msg;


	std::vector<unsigned char> buffer = jsonToBuffer(GET_QUESTION, j.dump().length(), j.dump());
	return buffer;
}

/*
Serialize (From JSON to buffer) LeaveGameResponse Response
input:  (LeaveGameResponse)	   lResp - The Response to serialize
output: (unsigned char vector) buffer - The Response after Serializing it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse lResp)
{
	nlohmann::json j;
	j["status"] = lResp.status;

	std::vector<unsigned char> buffer = jsonToBuffer(LEAVE_GAME, j.dump().length(), j.dump());
	return buffer;
}

//HELPER

/*
Creating the Form of the Serialized message
input:  (int)                  code - the id of the request type
		(int)                  length - the Length of the Content
		(string)               data - The Content
output: (unsigned char vector) buffer - The Message after sorting it
*/
std::vector<unsigned char> JsonResponsePacketSerializer::jsonToBuffer(int code, int length, std::string data)
{
	std::string buffDataStr;
	std::ostringstream zeroFiller;
	zeroFiller << std::setw(SIZE_BYTECOUNT) << std::setfill('0') << length; //fill blank places
	buffDataStr = std::to_string(code) + zeroFiller.str() + data;

	std::vector<unsigned char> buffer(buffDataStr.begin(), buffDataStr.end());
	return buffer;
}
