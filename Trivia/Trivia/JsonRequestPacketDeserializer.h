#pragma once
#include <string>
#include <vector>
#include "LoginStructs.h"
#include "RoomStructes.h"
#include "GameStructs.h"

static class JsonRequestPacketDeserializer
{
public:
	//Methods (VERSION 1)
	static LoginRequest				deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequest			deserializeSignupRequest(std::vector<unsigned char> buffer);

	//Methods (VERSION 2)
	static GetPlayersInRoomRequest  deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest			deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest		deserializeCreateRoomRequest(std::vector<unsigned char> buffer);
	static AddQuestionRequest		deserializeAddQuestionRequest(std::vector<unsigned char> buffer);

	//Methods (VERSION 3)
	static ChangeHandlerRequest     deserializeChangeHandlerRequest(std::vector<unsigned char> buffer);

	//Methods (VERSION 4) 
	static SubmitAnswerRequest      deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer);
};

