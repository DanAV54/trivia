#include "Communicator.h"

/*
Constructor
input: none
*/
Communicator::Communicator(RequestHandlerFactory& reqHand): m_handlerFactory(reqHand)
{
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
Destructor
input: none
*/
Communicator::~Communicator()
{
	try
	{
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

/*
Function that starts the communicator and handle requests by PORT
input:  none
output: none
*/
void Communicator::startHandleRequests()
{
	bindAndListen(PORT);
}

/*
Funcion that binds and listen to new Clients Joining the PORT
input:  (integer) port- Port to bind and listen by
output: none
*/
void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };
	int res;

	sa.sin_port		   = htons(port); 
	sa.sin_family	   = AF_INET;     
	sa.sin_addr.s_addr = INADDR_ANY;  

	//Listen & Bind- throw Exception if neccacery
	res = bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa));

	if (res == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "Listening on port " << port << std::endl;

	//Main thread accept new Clients
	while (true)
	{
		std::cout << CONNECTION_MSG << std::endl;
		acceptClient();
	}
}

/*
Accept new Client, Insdert it into the Socket|Handle Map & Creat new Thread for Client
input:  none
output: none
*/
void Communicator::acceptClient()
{
	//Accept the client- throw Exception if neccacery
	SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted." << std::endl;

	//Insert Client into Socket Map
	LoginRequestHandler* logReqHand = this->m_handlerFactory.createLoginRequestHandler();
	m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, (IRequestHandler*)logReqHand));

	//Open thread and a socket for the thread (client)
	std::thread handlerThread(&Communicator::handleNewClient, this, client_socket);
	handlerThread.detach();
}

/*
Handling with communication with client, sending receiving checking states and ect...
input:  (SOCKET) clientSocket- the specific SOCKET each thread need to take care of
output: none
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		RequestInfo reqInf;

		while (true)
		{
			reqInf = this->createRequestInfo(clientSocket);

			//Handle The Request
			bool relevant = this->m_clients[clientSocket]->isRequestRelevant(reqInf);
			if (relevant)
			{
				this->relevantReq(clientSocket, reqInf);
			}
			else
			{
				unrelevantReq(clientSocket);
			}
		}
	}
	catch (const std::exception&) //In case the SOCKET Disconnects Suprisly 
	{
		closesocket(clientSocket);
		auto it = this->m_clients.find(clientSocket);
		this->m_clients.erase(it);
		std::cout << "SOCKET CLOSED" << std::endl;
		return;
	}
}

/*
Taking values from client to create the RequestInfo and get all the neccacery details to it
input:  (SOCKET)      clientSocket- the socket to take\send the values from
output: (RequestInfo) reqInf- the full request info
*/
RequestInfo Communicator::createRequestInfo(SOCKET clientSocket)
{
	std::vector<unsigned char>   buffer;
	RequestInfo                  reqInf;
	std::string                  dataRec;
	int                          sizeInt;


	reqInf.receivalTime = time(0);
	dataRec				= this->recFromUser(clientSocket, ID_BYTES);
	reqInf.id			= atoi(dataRec.c_str());
	dataRec				= this->recFromUser(clientSocket, MSG_SIZE_BYTES);
	sizeInt				= atoi(dataRec.c_str());
	dataRec				= this->recFromUser(clientSocket, sizeInt);

	for (int i = 0; i < sizeInt; i++)
	{
		buffer.push_back(dataRec[i]);
	}
	reqInf.buffer = buffer;

	return reqInf;
}

/*
Receiving data that client sent through SOCKET
input:  (SOCKET)  clientSocket- the socket to take the values from
	    (Integer) size- the size to receive
output: (String)  dataString- the data that recieved
*/
std::string Communicator::recFromUser(SOCKET clientSocket, int size)
{
	std::string errString;
	char*       dateChar;

	dateChar	   = new char[size + 1];
	dateChar[size] = '\0';
	int res		   = recv(clientSocket, dateChar, size, 0);

	if (res == INVALID_SOCKET)
	{
		errString = "Error Recieving from User";
		std::cout << errString << std::endl;
		throw std::exception(errString.c_str());
	}

	std::string dataString(dateChar);
	delete[] dateChar;
	return dataString;
}

/*
Sending data to client through SOCKET
input:  (SOCKET) clientSocket- the socket to send the values to
		(char*)  sender- the function to send
output: none
*/
void Communicator::sendToUser(SOCKET clientSocket, char* sender)
{
	int   res;
	int   size;

	size = strlen(sender);
	res  = send(clientSocket, sender, size, 0);

	if (res == INVALID_SOCKET)
	{
		std::cout << "Error Sending To User" << std::endl;
		throw std::exception(ERROR_SENDING_MSG);
	}
}

/*
Handles with Relevent Request, switch the client state if neccacery and send Response to Client
input:  (SOCKET)       clientSocket- the socket to send the values to
		(RequestInfo)  reqInf- the data to send to client
output: none
*/
void Communicator::relevantReq(SOCKET clientSocket, RequestInfo reqInf)
{
	RequestResult reqRes;
	char*         dataSen;

	reqRes = this->m_clients[clientSocket]->handleRequest(reqInf);

	if (reqRes.newHandler != nullptr)
	{
		this->m_clients[clientSocket] = reqRes.newHandler;
	}

	dataSen							= new char[reqRes.response.size() + 1];
	dataSen[reqRes.response.size()] = '\0';

	for (int i = 0; i < reqRes.response.size(); i++)
	{
		dataSen[i] = reqRes.response[i];
	}
	std::cout << dataSen << std::endl; //DELETE
	this->sendToUser(clientSocket, dataSen);
	delete[] dataSen;
}

/*
Handles with UNrelevent Request (ERRORS), send Error to User
input:  (SOCKET)       clientSocket- the socket to send the values to
output: none
*/
void Communicator::unrelevantReq(SOCKET clientSocket)
{
	std::vector<unsigned char> buffer;
	ErrorResponse              err;
	char*                      dataSen;

	err.message			   = ERROR_RESPONSE_MSG;
	buffer				   = JsonResponsePacketSerializer::serializeResponse(err);
	dataSen				   = new char[buffer.size() + 1];
	dataSen[buffer.size()] = '\0';

	for (int i = 0; i < buffer.size(); i++)
	{
		dataSen[i] = buffer[i];
	}

	std::cout << dataSen << std::endl; //DELETE
	this->sendToUser(clientSocket, dataSen);
	delete[] dataSen;
}

/*
Setter for HandlerFactory
input:  (RequestHandlerFactory&) reqHand- the new value for m_handlerFactory
output: none
*/
void Communicator::setHandlerFactory(RequestHandlerFactory& reqHand)
{
	this->m_handlerFactory = reqHand;
}