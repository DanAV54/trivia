#pragma once
#include <vector>
#include <cstddef>
#include <bitset>
#include <iostream>
#include <sstream>
#include <iostream>
#include "LoginStructs.h"
#include "RoomStructes.h"
#include "GameStructs.h"

//VERSION 1 CODES
#define LOGIN_CODE			    10
#define SIGNUP_CODE			    11
#define ERROR_CODE			    12

//VERSION 2 CODES
#define LOGOUT_CODE				20
#define GET_ROOM				21
#define GET_PLAYERS_IN_ROOM		22
#define JOIN_ROOM				23
#define CREATE_ROOM				24
#define GET_HIGH_SCORE			25
#define GET_PERSONAL_STATS		26
#define ADD_QUESTION			27

//VERSION 3 CODES
#define CLOSE_ROOM_CODE		    30
#define START_GAME_CODE 		31
#define GET_STATE_CODE			32
#define LEAVE_ROOM_CODE		    33
#define CHANGE_HANDLER		    34

//VERSION 4 CODES
#define GET_GAME_RESULT		    40
#define SUBMIT_ANSWER			41
#define GET_QUESTION			42
#define LEAVE_GAME			    43

//OTHER DEFINES
#define SIZE_BYTECOUNT			4

static class JsonResponsePacketSerializer
{
public:
	//Methods (VERSION 1)
	static std::vector<unsigned char> serializeResponse(ErrorResponse eResp);
	static std::vector<unsigned char> serializeResponse(LoginResponse lResp);
	static std::vector<unsigned char> serializeResponse(SignupResponse sResp);

	//Methods (VERSION 2)
	static std::vector<unsigned char> serializeResponse(LogoutResponse lResp);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse gResp);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse gResp);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse jResp);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse cResp);
	static std::vector<unsigned char> serializeResponse(GetHighScoreResponse gResp);
	static std::vector<unsigned char> serializeResponse(GetPersonalStatsResponse gResp);
	static std::vector<unsigned char> serializeResponse(AddQuestionResponse aResp);

	//Methods (VERSION 3)
	static std::vector<unsigned char> serializeResponse(CloseRoomResponse cResp);
	static std::vector<unsigned char> serializeResponse(StartGameResponse sResp);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse gResp);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse lResp);
	static std::vector<unsigned char> serializeResponse(ChangeHandlerResponse cResp);

	//Methods (VERSION 4)
	static std::vector<unsigned char> serializeResponse(GetGameResultResponse gResp);
	static std::vector<unsigned char> serializeResponse(SubmitAnswerResponse sResp);
	static std::vector<unsigned char> serializeResponse(GetQuestionResponse gResp);
	static std::vector<unsigned char> serializeResponse(LeaveGameResponse lResp);

private:
	//Methods
	static std::vector<unsigned char> jsonToBuffer(int code, int length, std::string data);
};
