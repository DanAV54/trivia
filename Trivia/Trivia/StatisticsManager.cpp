#include "StatisticsManager.h"
#include <algorithm>

/*
Setter to database
input:  (IDatabase*) database- new value to m_database
output: (void)
*/
void StatisticsManager::setDataBase(IDatabase* database)
{
    m_database = database;
}

/*
getHighScore (first five players)
input:  (void)
output: (std::vector<std::pair<std::string, float>>) scores
*/
std::vector<std::pair<std::string, float>> StatisticsManager::getHighScore()
{
    std::vector<std::pair<std::string, float>> scores = getScores();
    std::pair<std::string, float>              temp;

    //bubble sort
    
    for (size_t i = 0; i < scores.size() - 1; ++i) {
        for (size_t j = 0; j < scores.size() - i - 1; ++j) {
            if (scores.at(j).second < scores.at(j + 1).second)
            {
                temp             = scores.at(j);
                scores.at(j)     = scores.at(j + 1);
                scores.at(j + 1) = temp;
            }
        }
    }

    //get the first 5
    while (scores.size() > 5)
        scores.pop_back();

    return scores;
}

/*
get players statistics
input:  (std::string)                  username- the user name to get the statics to
output: (std::map<std::string, float>) map- (data-value) the statics
*/
std::map<std::string, float> StatisticsManager::getUserStatistics(std::string username)
{
    std::map <std::string, float > map;
    map[std::string("correctAnswers")] = (float)m_database->getNumOfCorrectAnswers(username);
    map[std::string("playerGames")]    = (float)m_database->getNumOfPlayerGames(username);
    map[std::string("totalAnswers")]   = (float)m_database->getNumOfTotalAnswers(username);
    map[std::string("averageTime")]    = m_database->getPlayerAverageAnswerTime(username);
    return map;
}

/*
add a question to database
input:  (std::string) question- the question
        (std::string) curr_ans- correct answer
        (std::string) ans2
        (std::string) ans3
        (std::string) ans4
output: (void)
*/
void StatisticsManager::addQuestion(std::string question, std::string currAns, std::string ans2, std::string ans3, std::string ans4)
{
    this->m_database->addQuestion(question, currAns, ans2, ans3, ans4);
}

/*
submit question (add user statics)
input:  (Statistic) statics- the statics to add
output: (void)
*/
void StatisticsManager::submitQuestion(Statistic statics)
{
    m_database->addUserData(statics);
}

/* return the scores of all users. the score calc by the formula : 
({ avrage response time } / {currect answer})* { points possible }. max point is 100 for each question.
input:  (void) 
output: (std::vector<std::pair<std::string, float>>)- scores
*/
std::vector<std::pair<std::string, float>> StatisticsManager::getScores()
{
    std::vector<std::pair<std::string, float>> scores;
    float                                      responseAvgTime;
    int                                        numOfcurrAns;
    float                                      finalScore;

    auto users = m_database->getUsers();
    for (auto it = users.begin(); it != users.end(); it++)
    {
        finalScore = 0;
        responseAvgTime = m_database->getPlayerAverageAnswerTime(it->username);
        numOfcurrAns    = m_database->getNumOfCorrectAnswers(it->username);
        if(responseAvgTime != 0)
            finalScore = (numOfcurrAns / responseAvgTime) * 100;
        scores.push_back(std::pair<std::string, float>(it->username, finalScore));

    }
    return scores;
}
