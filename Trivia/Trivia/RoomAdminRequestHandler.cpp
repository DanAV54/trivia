#include "RoomAdminRequestHandler.h"

/*
Constructor
input: (RoomManager&)		    roomMan- value to roomManager
	   (RequestHandlerFactory&) reqHand- value to RequestHandlerFactory
	   (LoggedUser)				user- value to loggedUser
	   (Room*)					room- value to room
*/
RoomAdminRequestHandler::RoomAdminRequestHandler(RoomManager& roomMan, RequestHandlerFactory& reqHand, LoggedUser user, Room* room) : m_handlerFactory(reqHand), m_roomManager(roomMan), m_user(user), m_room(room)
{
}

/*
Checks if The Request is Relevent- Relevant Request is a req that matches the state the client is in now (Menu and so on)
input:  (RequestInfo) req- the request info keep the id inside and if the id matches room stuff so true
output: (Bool)        true/false- relevent/not relevent
*/
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
	return (req.id == CLOSE_ROOM_CODE ||
			req.id == START_GAME_CODE ||
			req.id == GET_STATE_CODE);
}

/*
Handle Request, and bridging between Code to Func, and return their result
input:  (RequestInfo)   req- the data of the request type 
output: (RequestResult) reqRes- the result from login/signup
*/
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;

	if (req.id == CLOSE_ROOM_CODE)
	{
		reqRes = this->closeRoom(req);
	}
	else if (req.id == START_GAME_CODE)
	{
		reqRes = this->startGame(req);
	}
	else if (req.id == GET_STATE_CODE)
	{
		reqRes = this->getRoomState(req);
	}
	return reqRes;
}

/*
close the Room, if it succeeded so send success and make the other users leave
input:  (RequestInfo)   req- the data of the closeRoom request
output: (RequestResult) reqRes- the result from closeRoom
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo req)
{
	RequestResult     reqRes;
	CloseRoomResponse closeRes;

	this->m_roomManager.deleteRoom((*m_room).getRoomId());

	closeRes.status   = POSITIVE_STATUS;
	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(closeRes);
	reqRes.newHandler = (IRequestHandler*)m_handlerFactory.createMenuRequestHandler(this->m_user);
	return reqRes;
}

/*
start the game, if it succeeded so send success and switch to game handler
input:  (RequestInfo)   req- the data of the startGame request
output: (RequestResult) reqRes- the result from startGame
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo req)
{
	RequestResult     reqRes;
	StartGameResponse startRes;
	RequestResult	  startResPlayers;
	Game*			  game;
	game = this->m_handlerFactory.getGameManager().createGame(m_room);
	
	(*m_room).setActive(2);

	startRes.status   = POSITIVE_STATUS;
	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(startRes);
	reqRes.newHandler = (IRequestHandler*)m_handlerFactory.createGameRequestHandler(m_user, game);
	return reqRes;
}

/*
get the room state, if it succeeded so send success and the room state
input:  (RequestInfo)   req- the data of the getRoomState request
output: (RequestResult) reqRes- the result from getRoomState
*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo req)
{
	RequestResult        reqRes;
	GetRoomStateResponse stateRes;
	bool                 flag;

	stateRes.hasGameBegun  = m_roomManager.getRoomState((*m_room).getRoomId());
	stateRes.players       = (*m_room).getAllUsers();
	stateRes.questionCount = (*m_room).getRoomQuestionCount();
	stateRes.status		   = POSITIVE_STATUS;
	stateRes.answerTimeout = (*m_room).getRoomTimePerQuestion();

	reqRes.response   = JsonResponsePacketSerializer::serializeResponse(stateRes);
	reqRes.newHandler = nullptr;
	return reqRes;
}
