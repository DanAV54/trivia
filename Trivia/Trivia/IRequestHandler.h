#pragma once
#include <vector>
#include <ctime>
#include <cstddef>

struct RequestResult;
struct RequestInfo
{
	int						   id;
	std::time_t				   receivalTime;
	std::vector<unsigned char> buffer;
};

class IRequestHandler
{
public:
	//Constructor
	IRequestHandler();

	//Destructor
	~IRequestHandler() = default;

	//Methods
	virtual bool		  isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo)	 = 0;
};


struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler*		   newHandler;
};
