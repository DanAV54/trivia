#include "LoginRequestHandler.h"


/*
Destructor
input: none
*/
LoginRequestHandler::LoginRequestHandler(LoginManager& logMan, RequestHandlerFactory& reqHand) : m_handlerFactory(reqHand), m_loginManager(logMan)
{
}

/*
Destructor
input: none
*/
LoginRequestHandler::~LoginRequestHandler()
{
}

/*
Checks if The Request is Relevent- Relevant Request is a req that matches the state the client is in now (Signing up and so on)
input:  (RequestInfo) req- the request info keep the id inside and if the id matches signup or login so true
output: (Bool)        true/false- relevent/not relevent
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo req)
{
    if (req.id == LOGIN_CODE || req.id == SIGNUP_CODE)
    {
        return true;
    }
    return false;
}

/*
Handle Request, if the request is LOGIN so send to login, if SIGNUP send to signup and return their result
input:  (RequestInfo)   req- the data of the login request type (log/sign)
output: (RequestResult) reqRes- the result from login/signup
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo req)
{
    RequestResult reqRes;

    if (req.id == LOGIN_CODE)
    {
        reqRes = this->login(req);
    }
    else if (req.id == SIGNUP_CODE)
    {
        reqRes = this->signup(req);
    }

    return reqRes;
}

/*
Logins the user, if it succeeded so send to next statment & send success 
input:  (RequestInfo)   req- the data of the login request
output: (RequestResult) reqRes- the result from login
*/
RequestResult LoginRequestHandler::login(RequestInfo req)
{
    RequestResult                 reqRes;
    LoginResponse                 logRes;
    LoginRequest                  logReq;
    bool                          flag;

    logReq = JsonRequestPacketDeserializer::deserializeLoginRequest(req.buffer);
    flag   = this->m_loginManager.login(logReq.username, logReq.password);

    if (flag)
    {
        logRes.status     = POSITIVE_STATUS;
        reqRes.response   = JsonResponsePacketSerializer::serializeResponse(logRes);
        m_loggedUser.setUsername(logReq.username);
        reqRes.newHandler = (IRequestHandler*)this->m_handlerFactory.createMenuRequestHandler(m_loggedUser);
    }
    else
    {
        logRes.status     = NEGATIVE_STATUS;
        reqRes.response   = JsonResponsePacketSerializer::serializeResponse(logRes);
        reqRes.newHandler = nullptr;
    }
    return reqRes;
}

/*
Signs up the user, if it succeeded so send to next statment & send success
input:  (RequestInfo)   req- the data of the signup request
output: (RequestResult) reqRes- the result from signup
*/
RequestResult LoginRequestHandler::signup(RequestInfo req)
{
    RequestResult                 reqRes;
    SignupResponse                sigRes;
    SignupRequest                 sigReq;
    bool                          flag;
    
    sigReq = JsonRequestPacketDeserializer::deserializeSignupRequest(req.buffer);
    flag   = this->m_loginManager.signup(sigReq.username, sigReq.password, sigReq.email);

    if (flag)
    {
        sigRes.status     = POSITIVE_STATUS;
        reqRes.response   = JsonResponsePacketSerializer::serializeResponse(sigRes);
        m_loggedUser.setUsername(sigReq.username);
        reqRes.newHandler = (IRequestHandler*)this->m_handlerFactory.createMenuRequestHandler(m_loggedUser);
    }
    else
    {
        sigRes.status     = NEGATIVE_STATUS;
        reqRes.response   = JsonResponsePacketSerializer::serializeResponse(sigRes);
        reqRes.newHandler = nullptr;
    }
    return reqRes;
}
