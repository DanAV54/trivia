#include "GameManager.h"

/*
Constructor
input: (IDatabase*) database- new value to m_database
*/
GameManager::GameManager(IDatabase* datebase)
{
	this->m_database = datebase;
}

/*
create a room and adds every player from the room to it
input:  (Room)  room- the room to create the game from
output: (Game*) game- the game
*/
Game* GameManager::createGame(Room* room)
{
	std::vector<Question>     questions;
	std::list<QuestionStruct> questionList = m_database->getQuestions();
	for (auto it = questionList.begin(); it != questionList.end(); it++)
	{
		questions.push_back(Question(*it));
	}

	std::map<LoggedUser, GameData> players;
	auto users   = room->getAllUsers();
	int  randNum = rand() % (m_database->getQuestions().size());
	auto que     = questionList.begin();

	for (int i = 0; i < randNum; i++)
		++que;
	for (auto it = users.begin(); it != users.end(); it++)
	{
		players[LoggedUser(*it)] = GameData{ *que , 0, 0, 0 };
		m_database->startGame(*it);
	}
	Game* game = new Game(questions, players, m_database, room->getRoomId());
	m_games.push_back(game);
	return game;
}

/*
delete a game from games
input:  (Game*) the game to delete
output: (void)
*/
void GameManager::deleteGame(Game* game)
{
	m_games.erase(std::find(m_games.begin(), m_games.end(), game));
	delete game;
}

/*
get the result of the game
input:  (Game*)						 game- the game to get the result of
output: (std::vector<PlayerResults>) results- the results
*/
std::vector<PlayerResults> GameManager::getResults(Game* game)
{
	std::vector<PlayerResults>     results;
	std::map<LoggedUser, GameData> map = game->getPlayers();
	for (auto it = map.begin(); it != map.end(); it++)
		results.push_back(PlayerResults{ (it->first).getUsername(), it->second.correctAnswerCount, it->second.wrongAnswerCount, it->second.averangeAnswerTime });
	return results;

}

/*
get a game by id
input:  (unsigned int) id- the id to check from
output: (Game*)		   [untitled]- the game
*/
Game* GameManager::getGame(unsigned int id)
{
	for (auto it = m_games.begin(); it != m_games.end(); it++)
	{
		if ((*it)->getGameId() == id)
			return (*it);
	}
}
