#pragma once
#include <string>
class LoggedUser
{
public:
	//Constructors
	LoggedUser();
	LoggedUser(std::string username);

	//Destructor
	~LoggedUser();

	//Getters
	std::string getUsername() const;

	//Setters
	void setUsername(std::string name);

	//operators
	bool operator <(const LoggedUser& other) const;
	bool operator >(const LoggedUser& other) const;

private:
	//Properties
	std::string m_username;
};
