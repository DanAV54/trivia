import socket
import json

def main():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(('127.0.0.1', 9090))
    flag = 0
    try:
        while flag == 0:
            res = input("Signup 0\nLogin 1\nLogout 2\nGet Room 3\ngetPlayerInRoom 4\njoinRoom 5\ncreatRoom 6\n"
                        "get_high_score 7\nget_presonal_statics 8\n")
            if res == '0':
                signup(client)
            elif res == '1':
                login(client)
            elif res == '2':
                logout(client)
            elif res == '3':
                getRoom(client)
            elif res == '4':
                getPlayerInRoom(client)
            elif res == '5':
                joinRoom(client)
            elif res == '6':
                creatRoom(client)
            elif res == '7':
                get_high_score(client)
            elif res == '8':
                get_presonal_statics(client)
            elif res == '9':
                flag = 1
            else:
                error(client)
    
    except Exception as e:
        print(e)
        client.close()
    client.close()


def get_high_score(client):
    msg = {

    }
    send(client, msg, 25)


def get_presonal_statics(client):
    msg = {

    }
    send(client, msg, 26)


def getPlayerInRoom(client):
    msg = {
        "roomId" : 0
    }
    send(client, msg, 22)


def joinRoom(client):
    msg = {
        "roomId" : 0
    }
    send(client, msg, 23)

def creatRoom(client):
    msg = {
        "answerTimeOut": 20,
        "maxUsers": 6,
        "questionCount": 10,
        "roomName": "coolRoom"
    }
    send(client, msg, 24)


def signup(client):
    signupMassage = {
        "username": "aaaa",
        "password": "1234",
        "email": "user1@gmail.com"
    }
    send(client, signupMassage, 11)


def login(client):
    name = "aaaa"  # input("Enter Name\n")
    password = "1234"  # input("Enter Pass\n")
    loginMassage = {
        "username": name,
        "password": password
    }
    send(client, loginMassage, 10)


def error(client):
    errorMassage = {
        "username": "user1",
        "password": "1234"
    }
    send(client, errorMassage, 0)


def logout(client):
    logoutMassage = {
    }
    send(client, logoutMassage, 20)

def getRoom(client):
    getRoom = {
         "roomId" : 1

        }
    send(client, getRoom, 21)


def send(client, message, code):
    try:
        length = str(len(json.dumps(message))).zfill(4)
        msg = str(code) + length + json.dumps(message)
        print(msg)
        client.send(msg.encode())
        print(client.recv(100).decode())

    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()
