﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace GUI_Trivia
{
    public partial class SignupWindow : Window
    {
        SocketClass client; //Socket

        //Constructor
        public SignupWindow(SocketClass client)
        {
            InitializeComponent();
            this.client = client;
        }

        /*
        Tries to SignUp (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void signUp(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Text = "";
            SignupReq signReq = new SignupReq
            {
                username = username.Text,
                password = password.Text,
                email = mail.Text
            };
            if(signReq.email == "" || signReq.username == "" || signReq.password == "") //if no data was written
            {
                addErrorMessage(1);
                return;
            }

            string jsonString = JsonConvert.SerializeObject(signReq);
            string msg = Constants.SIGNUP_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            SignupRes signRes = JsonConvert.DeserializeObject<SignupRes>(received.Substring(6));
            if (signRes.status != 1) //Send Error
            {
                addErrorMessage(2);
            }
            else //Open MainMenu
            {
                MainMenu mainMenu = new MainMenu(client);
                mainMenu.Show();
                this.Close();
            }
        }

        /*
        Switch to Login Window
        input:  (button info) sender, e
        output: (void)
        */
        private void login(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWind = new LoginWindow(client);
            loginWind.Show();
            this.Close();
        }

        /*
        Adds Error If SignUp Failed
        input:  (Integer) type- the error message to write
        output: (void)
        */
        private void addErrorMessage(int type)
        {
            switch (type)
            {
                case 1:
                    ErrorMsg.Text = "Please Make Sure You Entered Everything!";
                    break;

                case 2:
                    ErrorMsg.Text = "User already Exist!";
                    break;
            }
        }

        /*
        Ends the Program
        input:  (button info) sender, e
        output: (void)
        */
        private void Quit(object sender, RoutedEventArgs e)
        {
            this.client.GetClient().Close();
            this.Close();
        }
    }
}
