﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace GUI_Trivia
{
    public partial class LoginWindow : Window
    {
        SocketClass client; //Socket

        //Constructor initalizing program
        public LoginWindow()
        {
            InitializeComponent();
            this.client = new SocketClass();
        }

        //Constructor For Switching windows
        public LoginWindow(SocketClass client)
        {
            InitializeComponent();
            this.client = client;
        }

        /*
        Tries to SignIn (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void signIn(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Text = "";
            LoginReq logReq = new LoginReq
            {
                username = username.Text,
                password = password.Text
            };
            if (logReq.username == "" || logReq.password == "") //if no data was written
            {
                addErrorMessage(1);
                return;
            }
            if (logReq.username.Length > 8 || logReq.password.Length > 8) //if no data was written
            {
                addErrorMessage(3);
                return;
            }

            string jsonString = JsonConvert.SerializeObject(logReq);
            string msg = Constants.LOGIN_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();
            Console.WriteLine("{0}", received);
            LoginRes logRes = JsonConvert.DeserializeObject<LoginRes>(received.Substring(6));
            if (logRes.status != 1) //Send Error
            {
                addErrorMessage(2);
            }
            else //Open MainMenu
            {
                MainMenu mainMenu = new MainMenu(client);
                mainMenu.Show();
                this.Close();
            }
        }

        /*
        Switch to signUp Window
        input:  (button info) sender, e
        output: (void)
        */
        private void SignUp(object sender, RoutedEventArgs e)
        {
            SignupWindow signupWind = new SignupWindow(client);
            signupWind.Show();
            this.Close();
        }

        /*
        Adds Error If SignIn Failed
        input:  (Integer) type- the error message to write
        output: (void)
        */
        private void addErrorMessage(int type)
        {
            switch (type)
            {
                case 1:
                    ErrorMsg.Text = "Please Make Sure You Entered Everything!";
                    break;

                case 2:
                    ErrorMsg.Text = "User Does'nt Exist! (OR ALREADY LOGGED IN)";
                    break;

                case 3:
                    ErrorMsg.Text = "Too Long";
                    break;
            }
        }

        /*
        Ends the Program
        input:  (button info) sender, e
        output: (void)
        */
        private void Quit(object sender, RoutedEventArgs e)
        {
            this.client.GetClient().Close();
            this.Close();
        }
    }
}
