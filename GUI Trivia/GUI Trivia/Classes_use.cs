﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    public class SocketClass
    {
        TcpClient client;
        public SocketClass()
        {
            this.client = new TcpClient("127.0.0.1", 9090);
        }
        public TcpClient GetClient()
        {
            return this.client;
        }

        public void send(string msg)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(msg);
            NetworkStream stream = this.client.GetStream();
            stream.Write(data, 0, data.Length);
        }

        public string receive()
        {
            NetworkStream stream = this.client.GetStream();
            Byte[] data = new Byte[1028];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            return responseData;
        }
    }

    public class RoomData { public int id; public string name; public int maxPlayers; public int numOfQuestionInGame; public int timePerQuestion; public int isActive; }

    //VERSION 1
    public class LoginReq { public string username; public string password; }
    public class LoginRes { public int status; }
    public class SignupReq { public string username; public string password; public string email; }
    public class SignupRes { public int status; }

    //VERSION 2
    public class LogoutRes { public int status; }
    public class GetHighScoreRes { public string statics; public int status; }
    public class GetePersonalStatsRes { public int status; public string statics; }
    public class CreateRoomReq { public string roomName; public int maxUsers; public int questionCount; public int answerTimeOut; }
    public class CreateRoomRes { public int status; public int roomId; }
    public class GetRoomsRes { public int status; public string Rooms; }
    public class GetPlayersInRoomReq { public int roomId; }
    public class GetPlayersInRoomRes { public string PlayersInRoom; }
    public class JoinRoomReq { public int roomId; }
    public class JoinRoomRes { public int status; }
    public class AddQuestionReq { public string question; public string currAns; public string ans2; public string ans3; public string ans4; }
    public class AddQuestionRes { public int status; }

    //VERSION 3
    public class StartGameRes { public int status; }
    public class CloseGameRes { public int status; }
    public class LeaveRoomRes { public int status; }
    public class GetRoomStateRes { public int status; public int hasGameBegun; public string players; public int questionCount; public int answerTimeout; }
    public class ChangeHandlerReq { public int newHandler;}
    public class ChangeHandlerRes { public int status; }

    //VERSION 4
    public class SubmitAnswerReq { public int answerId; public int timer; }
    public class SubmitAnswerRes { public int status; public int correctAnswerId; }
    public class LeaveGameRes { public int status;}
    public class GetQuestionRes { public int status; public string question; public string answers; }
    public class GetGameRes { public int status; public string results;}

    //HELPER
    public class Results { public string username; public int corrAns; public int wrongAns; public float avg; }
    static class Constants
    {
        //VERSION 1 CODES
        public const int LOGIN_CODE = 10;
        public const int SIGNUP_CODE = 11;
        public const int ERROR_CODE = 12;

        //VERSION 2 CODES
        public const int LOGOUT_CODE = 20;
        public const int GET_ROOM = 21;
        public const int GET_PLAYERS_IN_ROOM = 22;
        public const int JOIN_ROOM = 23;
        public const int CREATE_ROOM = 24;
        public const int GET_HIGH_SCORE = 25;
        public const int GET_PERSONAL_STATS = 26;
        public const int ADD_QUESTION = 27;

        //VERSION 3 CODES
        public const int CLOSE_ROOM_CODE = 30;
        public const int START_GAME_CODE = 31;
        public const int GET_STATE_CODE = 32;
        public const int LEAVE_ROOM_CODE = 33;
        public const int CHANGE_HANDLER = 34;

        //VERSION 4 CODES
        public const int GET_GAME_RESULT = 40;
        public const int SUBMIT_ANSWER = 41;
        public const int GET_QUESTION =	42;
        public const int LEAVE_GAME	= 43;
    }
}
