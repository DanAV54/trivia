﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    /// <summary>
    /// Interaction logic for staticsWindow.xaml
    /// </summary>
    public partial class staticsWindow : Window
    {
        SocketClass client; //Socket

        //Constructor + Statics recieve from server
        public staticsWindow(SocketClass client)
        {
            InitializeComponent();
            this.client = client;

            string jsonString = "{}";
            string msg = Constants.GET_HIGH_SCORE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            //HGHSCORE
            GetHighScoreRes HighRes = JsonConvert.DeserializeObject<GetHighScoreRes>(received.Substring(6));
            string[] statsPlayers = JsonConvert.DeserializeObject<string[]>(HighRes.statics);
            highscore1.Text = statsPlayers[0].Substring(0, statsPlayers[0].IndexOf(",")) + "    " + statsPlayers[0].Substring(statsPlayers[0].IndexOf(",") + 1);
            highscore2.Text = statsPlayers[1].Substring(0, statsPlayers[1].IndexOf(",")) + "    " + statsPlayers[1].Substring(statsPlayers[1].IndexOf(",") + 1);
            highscore3.Text = statsPlayers[2].Substring(0, statsPlayers[2].IndexOf(",")) + "    " + statsPlayers[2].Substring(statsPlayers[2].IndexOf(",") + 1);
            highscore4.Text = statsPlayers[3].Substring(0, statsPlayers[3].IndexOf(",")) + "    " + statsPlayers[3].Substring(statsPlayers[3].IndexOf(",") + 1);
            highscore5.Text = statsPlayers[4].Substring(0, statsPlayers[4].IndexOf(",")) + "    " + statsPlayers[4].Substring(statsPlayers[4].IndexOf(",") + 1);

            jsonString = "{}";
            msg = Constants.GET_PERSONAL_STATS + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            received = this.client.receive();

            //PERSONAL STATS
            GetePersonalStatsRes personalStatRes = JsonConvert.DeserializeObject<GetePersonalStatsRes>(received.Substring(6));
            float[] personalArr = JsonConvert.DeserializeObject<float[]>(personalStatRes.statics);
            CorrectAns.Text  = personalArr[0].ToString();
            gamesPlayed.Text = personalArr[1].ToString();
            TotalAns.Text    = personalArr[2].ToString();
            Avg.Text         = personalArr[3].ToString();
        }

        /*
        Switch to MainMenu Window
        input:  (button info) sender, e
        output: (void)
        */
        private void Back(object sender, RoutedEventArgs e)
        {
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

    }
}
