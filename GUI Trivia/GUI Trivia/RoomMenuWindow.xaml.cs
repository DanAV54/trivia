﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    /// <summary>
    /// Interaction logic for RoomMenuWindow.xaml
    /// </summary>
    public partial class RoomMenuWindow : Window
    {
        SocketClass client; //Socket
        int status; //User or Admin
        private BackgroundWorker background_worker;
        RoomData data;

        //Constructor + GetPlayersInRoom
        public RoomMenuWindow(SocketClass client, RoomData roomData, int status)
        {
            InitializeComponent();
            this.client = client;
            RoomName.Text = roomData.name;
            this.status = status;
            data = roomData;
            if (this.status == 1)
            {
                Close.Content = "CLOSE ROOM";
            }
            else
            {
                Start.IsEnabled = false;
                Start.Background = Brushes.Gray;
                Close.Content = "LEAVE ROOM";
            }

            background_worker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            background_worker.DoWork += Background_worker_DoWork;
            background_worker.ProgressChanged += Background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += Background_worker_RunWorkerCompleted; ;
            background_worker.RunWorkerAsync();
        }

        /*
        refreshing screen
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                background_worker.ReportProgress(1);
                Thread.Sleep(3000);
            }
        }

        /*
        updating screen
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            GetRoomStateRes res = getRoomState();
            if (res.hasGameBegun == 1)
            {
                Players.Text = res.players;
            }
            else if(res.hasGameBegun == 2)
            {
                ChangeHandle(2);
                SwitchToGame();
            }
            else if (res.hasGameBegun == 0)
            {
                ChangeHandle(1);
                SwitchToMenu();
            }
        }

        /*
        get result/exit
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        /*
        close/leave room
        input:  (button info) sender, e
        output: (void)
        */
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (status == 1) //admin
            {
                string jsonString = "{}";
                string msg = Constants.CLOSE_ROOM_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;
                Console.WriteLine("{0}", msg);
                this.client.send(msg);
                string received = this.client.receive();

                CloseGameRes getPlayerInRoom = JsonConvert.DeserializeObject<CloseGameRes>(received.Substring(6));
            }
            else //member
            {
                string jsonString = "{}";
                string msg = Constants.LEAVE_ROOM_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

                this.client.send(msg);
                string received = this.client.receive();

                LeaveRoomRes getPlayerInRoom = JsonConvert.DeserializeObject<LeaveRoomRes>(received.Substring(6));
            }
            background_worker.CancelAsync();
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        start game
        input:  (button info) sender, e
        output: (void)
        */
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (status == 1)
            {
                string jsonString = "{}";
                string msg = 31 + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

                this.client.send(msg);
                string received = this.client.receive();

                StartGameRes getPlayerInRoom = JsonConvert.DeserializeObject<StartGameRes>(received.Substring(6));
                background_worker.CancelAsync();
                GameWindow gameWind = new GameWindow(client, data);
                gameWind.Show();
                this.Close();
            }
        }

        /*
        change handle to game or menu (member func)
        input:  (int)  newHandler- handle to switch to
        output: (void)
        */
        void ChangeHandle(int newHandler)
        {
            ChangeHandlerReq changeReq = new ChangeHandlerReq
            {
                newHandler = newHandler
            };
            string jsonString = JsonConvert.SerializeObject(changeReq);
            string msg = 34 + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            ChangeHandlerRes changeRes = JsonConvert.DeserializeObject<ChangeHandlerRes>(received.Substring(6));
        }

        /*
        get the room state
        input:  (void)  
        output: (GetRoomStateRes) getRoomState- room state
        */
        GetRoomStateRes getRoomState()
        {
            string jsonString = "{}";
            string msg = Constants.GET_STATE_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            GetRoomStateRes getRoomState = JsonConvert.DeserializeObject<GetRoomStateRes>(received.Substring(6));
            return getRoomState;
        }

        /*
        switch to menu window
        input:  (void)  
        output: (void)
        */
        void SwitchToMenu()
        {
            background_worker.CancelAsync();
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        switch to game window
        input:  (void)  
        output: (void)
        */
        void SwitchToGame()
        {
            background_worker.CancelAsync();
            GameWindow gameWind = new GameWindow(client, data);
            gameWind.Show();
            this.Close();
        }
    }
}
