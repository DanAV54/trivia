﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace GUI_Trivia
{
    public partial class CreateRoomWindow : Window
    {
        SocketClass client; //Socket

        //Constructor
        public CreateRoomWindow(SocketClass client)
        {
            InitializeComponent();
            this.client = client;
        }

        /*
        Tries to CreateRoom (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void Create(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Text = ""; //Clean MSG ERROR
            if (RoomName.Text == "" || QuestionCount.Text == "" || MaxPlayers.Text == "" || TimePerAnswer.Text == "")
            {
                addErrorMessage(1);
                return;
            } //Check if all te data was writen
            RoomData roomDat = new RoomData
            {
                name = RoomName.Text,
                maxPlayers = Int32.Parse(MaxPlayers.Text),
                numOfQuestionInGame = Int32.Parse(QuestionCount.Text),
                timePerQuestion = Int32.Parse(TimePerAnswer.Text),
                isActive = 1
            };
            CreateRoomReq CreaReq = new CreateRoomReq
            {
                roomName = RoomName.Text,
                maxUsers = Int32.Parse(MaxPlayers.Text),
                questionCount = Int32.Parse(QuestionCount.Text),
                answerTimeOut = Int32.Parse(TimePerAnswer.Text)
            };

            //Check Input Errors
            if (CreaReq.maxUsers > 10 || CreaReq.maxUsers < 2)
            {
                addErrorMessage(2);
                return;
            }
            if (CreaReq.questionCount > 50 || CreaReq.questionCount < 5)
            {
                addErrorMessage(3);
                return;
            } 
            if (CreaReq.answerTimeOut < 10 || CreaReq.answerTimeOut > 120)
            {
                addErrorMessage(4);
                return;
            }

            string jsonString = JsonConvert.SerializeObject(CreaReq);
            string msg = Constants.CREATE_ROOM + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            CreateRoomRes createRoomRes = JsonConvert.DeserializeObject<CreateRoomRes>(received.Substring(6));
            if (createRoomRes.status != 1)
            {
                ErrorMsg.Text = "Error Create Room";
            }
            else
            {
                roomDat.id = createRoomRes.roomId;
                RoomMenuWindow roomWind = new RoomMenuWindow(client, roomDat ,1);
                roomWind.Show();
                this.Close();
            }
        }

        /*
        Switch to MainMenu Window
        input:  (button info) sender, e
        output: (void)
        */
        private void Back(object sender, RoutedEventArgs e)
        {
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        Allows Only Numbers in TextBox
        input:  (button info) sender, e
        output: (void)
        */
        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /*
        Adds Error If SignUp Failed
        input:  (Integer) type- the error message to write
        output: (void)
        */
        private void addErrorMessage(int type)
        {
            switch (type)
            {
                case 1:
                    ErrorMsg.Text = "Please Make Sure You Entered Everything!";
                    break;

                case 2:
                    ErrorMsg.Text = "Max 10 Players and at least 2";
                    break;

                case 3:
                    ErrorMsg.Text = "Max 50 Questions and at least 5";
                    break;

                case 4:
                    ErrorMsg.Text = "Max 180 seconds and at least 10";
                    break;
            }
        }
    }
}
