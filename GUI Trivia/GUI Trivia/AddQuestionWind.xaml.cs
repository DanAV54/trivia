﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    public partial class AddQuestionWind : Window
    {
        SocketClass client; //Socket

        //Constructor
        public AddQuestionWind(SocketClass client)
        {
            InitializeComponent();
            this.client = client;
        }

        /*
        Switch to MainMenu Window
        input:  (button info) sender, e
        output: (void)
        */
        private void Back(object sender, RoutedEventArgs e)
        {
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        Tries to CreateRoom (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void add(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Text = ""; //Clean MSG ERROR
            if (Question.Text == "" || CurrAns.Text == "" || Ans2.Text == "" || Ans3.Text == "" || Ans4.Text == "")
            {
                addMessage(1);
                return;
            } //Check if all te data was writen
            AddQuestionReq addQuest = new AddQuestionReq
            {
                question = Question.Text,
                currAns = CurrAns.Text,
                ans2 = Ans2.Text,
                ans3 = Ans3.Text,
                ans4 = Ans4.Text
            };

            string jsonString = JsonConvert.SerializeObject(addQuest);
            string msg = Constants.ADD_QUESTION + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            AddQuestionRes createRoomRes = JsonConvert.DeserializeObject<AddQuestionRes>(received.Substring(6));
            addMessage(2);
        }

        /*
        adds an message by type
        input:  (int) type- the message type
        output: (void)
        */
        private void addMessage(int type)
        {
            switch (type)
            {
                case 1:
                    ErrorMsg.Foreground = Brushes.Red;
                    ErrorMsg.Text = "Please Make Sure You Entered Everything!";
                    break;

                case 2:
                    ErrorMsg.Foreground = Brushes.Green;
                    ErrorMsg.Text = "Question Added Succecfuly";
                    break;
            }
        }
    }
}
