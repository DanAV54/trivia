﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    public partial class GameResultWindow : Window
    {
        SocketClass client; //socket

        //Constructor
        public GameResultWindow(SocketClass client, string roomName, Results[] gameResults)
        {
            InitializeComponent();
            RoomName.Text = roomName;
            TextBlock[] res = { results1, results2,results3,results4,results5,results6,results7,results8,results9,results10};
            this.client = client;
            for (int i = 0; i < gameResults.Length; i++)
            {

            }
            //bubble sort winner
            Results temp;
            for (int j = 0; j <= gameResults.Length - 2; j++)
            {
                for (int i = 0; i <= gameResults.Length - 2; i++)
                {
                    if((gameResults[i].corrAns / gameResults[i].avg) < (gameResults[i+1].corrAns / gameResults[i + 1].avg))
                    {
                        temp = gameResults[i + 1];
                        gameResults[i + 1] = gameResults[i];
                        gameResults[i] = temp;
                    }
                }
            }

            //add to screen
            for (int i = 0; i < gameResults.Length; i++)
            {
                res[i].Text = gameResults[i].username + "          " + gameResults[i].corrAns.ToString() + "          " + gameResults[i].wrongAns.ToString() + "          " + gameResults[i].avg.ToString();
            }
        }

        /*
        leave the game
        input:  (button info) sender, e
        output: (void)
        */
        private void Leave(object sender, RoutedEventArgs e)
        {
            string jsonString = "{}";
            string msg = Constants.LEAVE_GAME + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            LeaveGameRes leaveGame = JsonConvert.DeserializeObject<LeaveGameRes>(received.Substring(6));
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }
    }
}
