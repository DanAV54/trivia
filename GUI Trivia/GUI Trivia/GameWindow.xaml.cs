﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    public partial class GameWindow : Window
    {
        SocketClass client; //socket
        private BackgroundWorker background_worker; //worker
        RoomData data; //room data (from before>
        int currQuest; //current question
        int currScore; //current score
        int flag; //flag to see if submited
        bool clickable; //enable buttons (without losing colors ;)

        DispatcherTimer _timer; //timers
        TimeSpan _time;

        //Constructor
        public GameWindow(SocketClass client, RoomData roomData)
        {
            InitializeComponent();
            this.client = client;
            RoomName.Text = roomData.name;
            data = roomData;
            
            currQuest = 1;
            currScore = 0;
            flag = -1;
            clickable = true;

            background_worker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            background_worker.DoWork += Background_worker_DoWork;
            background_worker.ProgressChanged += Background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += Background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        /*
        playing the game
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(currQuest - 1 != data.numOfQuestionInGame)
            {
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                background_worker.ReportProgress(1);
                Thread.Sleep(1000 * (data.timePerQuestion +1 ));
                _timer.Stop();
                currQuest++;
            }
        }

        /*
        screen change from question to question
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if(flag == 0) //if no  answer was selected
            {
                submitAns(0);
            }
            flag = 0;
            GetQuestionRes res = GetQuestion(); //wait fro question
            Dictionary<int, string> dict = JsonConvert.DeserializeObject<Dictionary<int, string>>(res.answers);
            //diffult setting before answering
            clickable = true;
            ans1.Background = Brushes.Red;
            ans2.Background = Brushes.Blue;
            ans3.Background = Brushes.Yellow;
            ans4.Background = Brushes.Green;
            questionCount.Text = currQuest.ToString() + "/" + data.numOfQuestionInGame.ToString();
            score.Text = currScore.ToString() + "/" + currQuest.ToString();
            question.Text = res.question;
            ans1.Content = dict[1];
            ans2.Content = dict[2];
            ans3.Content = dict[3];
            ans4.Content = dict[4];

            _time = TimeSpan.FromSeconds(data.timePerQuestion);
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                Time.Text = _time.ToString("c");
                if (_time == TimeSpan.Zero) _timer.Stop();
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);

            _timer.Start();
        }

        /*
        get result or leave
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                return;
            }
            string jsonString = "{}";
            string msg = Constants.GET_GAME_RESULT + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            GetGameRes getGameRes = JsonConvert.DeserializeObject<GetGameRes>(received.Substring(6));
            Results[] res = JsonConvert.DeserializeObject<Results[]>(getGameRes.results);
            results(res);
        }

        /*
        get current question
        input:  (void)
        output: (GetQuestionRes) getQuestion- the question
        */
        private GetQuestionRes GetQuestion()
        {
            string jsonString = "{}";
            string msg = Constants.GET_QUESTION + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            GetQuestionRes getQuestion = JsonConvert.DeserializeObject<GetQuestionRes>(received.Substring(6));
            return getQuestion;
        }

        /*
        leave game
        input:  (button info) sender, e
        output: (void)
        */
        private void exit(object sender, RoutedEventArgs e)
        {
            string jsonString = "{}";
            string msg = Constants.LEAVE_GAME + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            background_worker.CancelAsync();
            LeaveGameRes leaveGame = JsonConvert.DeserializeObject<LeaveGameRes>(received.Substring(6));
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        switch to result window
        input:  (Results[]) results- result for resWindow constructor
        output: (void)
        */
        private void results(Results[] results)
        {
            background_worker.CancelAsync();
            GameResultWindow gameResWind = new GameResultWindow(client, this.data.name, results);
            gameResWind.Show();
            this.Close();
        }

        /*
        answer 1 was chosen
        input:  (button info) sender, e
        output: (void)
        */
        private void answer1(object sender, RoutedEventArgs e)
        {
            if (clickable == false)
            {
                return;
            }
            SubmitAnswerRes subRes = submitAns(1);
            ans2.Background = Brushes.Gray;
            ans3.Background = Brushes.Gray;
            ans4.Background = Brushes.Gray;
            if (subRes.correctAnswerId == 1)
            {
                currScore++;
                score.Text = currScore.ToString() + "/" + currQuest.ToString();
                ans1.Background = Brushes.Green;
            }
            else
            {
                ans1.Background = Brushes.Red;
            }
        }

        /*
        answer 2 was chosen
        input:  (button info) sender, e
        output: (void)
        */
        private void answer2(object sender, RoutedEventArgs e)
        {
            if (clickable == false)
            {
                return;
            }
            SubmitAnswerRes subRes = submitAns(2);
            ans1.Background = Brushes.Gray;
            ans3.Background = Brushes.Gray;
            ans4.Background = Brushes.Gray;
            if (subRes.correctAnswerId == 2)
            {
                currScore++;
                score.Text = currScore.ToString() + "/" + currQuest.ToString();
                ans2.Background = Brushes.Green;
            }
            else
            {
                ans2.Background = Brushes.Red;
            }
        }

        /*
        answer 3 was chosen
        input:  (button info) sender, e
        output: (void)
        */
        private void answer3(object sender, RoutedEventArgs e)
        {
            if (clickable == false)
            {
                return;
            }
            SubmitAnswerRes subRes = submitAns(3);
            ans1.Background = Brushes.Gray;
            ans2.Background = Brushes.Gray;
            ans4.Background = Brushes.Gray;
            if (subRes.correctAnswerId == 3)
            {
                currScore++;
                score.Text = currScore.ToString() + "/" + currQuest.ToString();
                ans3.Background = Brushes.Green;
            }
            else
            {
                ans3.Background = Brushes.Red;
            }
        }

        /*
        answer 4 was chosen
        input:  (button info) sender, e
        output: (void)
        */
        private void answer4(object sender, RoutedEventArgs e)
        {
            if (clickable == false)
            {
                return;
            }
            SubmitAnswerRes subRes = submitAns(4);
            ans1.Background = Brushes.Gray;
            ans2.Background = Brushes.Gray;
            ans3.Background = Brushes.Gray;
            if (subRes.correctAnswerId == 4)
            {
                currScore++;
                score.Text = currScore.ToString() + "/" + currQuest.ToString();
                ans4.Background = Brushes.Green;
            }
            else
            {
                ans4.Background = Brushes.Red;
            }
        }

        /*
        submit answer
        input:  (int)             ans- which answer submit
        output: (SubmitAnswerRes) subRes- result
        */
        SubmitAnswerRes submitAns(int ans)
        {
            flag = ans;
            SubmitAnswerReq subReq = new SubmitAnswerReq
            {
                answerId = ans,
                timer = int.Parse(_time.Seconds.ToString())
            };
            string jsonString = JsonConvert.SerializeObject(subReq);
            string msg = Constants.SUBMIT_ANSWER + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            SubmitAnswerRes subRes = JsonConvert.DeserializeObject<SubmitAnswerRes>(received.Substring(6));
            clickable = false;
            return subRes;
        }
    }
}
