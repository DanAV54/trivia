﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Net.Sockets;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    public partial class MainMenu : Window
    {
        SocketClass client; //Socket

        //Constructor
        public MainMenu(SocketClass client)
        {
            InitializeComponent();
            this.client = client;
        }

        /*
        Switch to JoinRoom Window
        input:  (button info) sender, e
        output: (void)
        */
        private void joinRoom(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow joinWind = new JoinRoomWindow(client);
            joinWind.Show();
            this.Close();
        }

        /*
        Switch to CreateRoom Window
        input:  (button info) sender, e
        output: (void)
        */
        private void createRoom(object sender, RoutedEventArgs e)
        {
            CreateRoomWindow creRoomWindw = new CreateRoomWindow(client);
            creRoomWindw.Show();
            this.Close();
        }

        /*
        Switch to Statics Window
        input:  (button info) sender, e
        output: (void)
        */
        private void statics(object sender, RoutedEventArgs e)
        {
            staticsWindow stat= new staticsWindow(client);
            stat.Show();
            this.Close();
        }

        /*
        Signout and Switch to Login Window
        input:  (button info) sender, e
        output: (void)
        */
        private void signOut(object sender, RoutedEventArgs e)
        {
            LogoutRes logRes = logout();
            if (logRes.status == 0)
            {
                ErrorMsg.Text = "Error Loging Out";
            }
            else
            {
                LoginWindow loginWindow = new LoginWindow(client);
                loginWindow.Show();
                this.Close();
            }
        }

        /*
        Ends the Program
        input:  (button info) sender, e
        output: (void)
        */
        private void quit(object sender, RoutedEventArgs e)
        {
            logout();
            this.client.GetClient().Close();
            this.Close();
        }

        /*
        add a question
        input:  (button info) sender, e
        output: (void)
        */
        private void addQuestion(object sender, RoutedEventArgs e)
        {
            AddQuestionWind addWind = new AddQuestionWind(client);
            addWind.Show();
            this.Close();
        }

        /*
        Logout and sends to server
        input:  (void)
        output: (LogoutRes) logRes- the data from the server (about the logout)
        */
        private LogoutRes logout()
        {
            string jsonString = "{}";
            string msg = Constants.LOGOUT_CODE + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            LogoutRes logRes = JsonConvert.DeserializeObject<LogoutRes>(received.Substring(6));
            return logRes;
        }
    }
}
