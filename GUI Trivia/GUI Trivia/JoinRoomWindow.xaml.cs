﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;

namespace GUI_Trivia
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        SocketClass client; //Socket
        RoomData[] rooms; //All The Rooms
        int current_id; //Current Id Chosen
        private BackgroundWorker background_worker;

        //Constructor + GetRooms
        public JoinRoomWindow(SocketClass client)
        {
            InitializeComponent();
            //mediaADD.Source = new Uri(Environment.CurrentDirectory + @"Media/warGif.mp4");
            grid1.Children.Clear();
            this.client = client;
            current_id = -1;

            background_worker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            background_worker.DoWork             += Background_worker_DoWork;  
            background_worker.ProgressChanged    += Background_worker_ProgressChanged;   
            background_worker.RunWorkerCompleted += Background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        /*
        get all rooms
        input:  (void)
        output: (GetRoomsRes) GetRoomsRes- the rooms
        */
        GetRoomsRes getRooms()
        {
            string jsonString = "{}";
            string msg = Constants.GET_ROOM + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            GetRoomsRes GetRoomsRes = JsonConvert.DeserializeObject<GetRoomsRes>(received.Substring(6));
            return GetRoomsRes;
        }

        /*
        refreshing screen
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                background_worker.ReportProgress(1);
                Thread.Sleep(3000);
            }
        }

        /*
        update screen
        input:  (worker info) sender, e
        output: (void)
        */
        private void Background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            GetRoomsRes res = getRooms();
            roomsAdding(res.Rooms);
        }

        //empty func
        private void Background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        /*
        Create Buttons to all the rooms with the neccacery data (id and ect...)
        input:  (string) recv- the data of the getRoomRequest
        output: (void)
        */
        private void roomsAdding(string recv)
        {
            Rooms.Text = "";
            grid1.Children.Clear();
            if (recv.CompareTo("[]") == 0)
            {
                Rooms.Text = "NO ROOMS ARE AVAIBLE";
                return;
            }
            Grid panel = new Grid();
            Console.WriteLine("{0}", recv);
            rooms = JsonConvert.DeserializeObject<RoomData[]>(recv);
            int flag = 0;
            for (int i = 200; i < 650 && flag == 0; i += 100)
            {
                for (int j = 300; j < 1200 && flag == 0; j += 150)
                {
                    for(int k = 0; k < rooms.Length; k++)
                    {
                        if (rooms[k].isActive != 2)
                        {
                            //ADD BUTTON TO THE ROOM
                            Button but = new Button();
                            but.Margin = new Thickness(j, i, 1600 - j - 150, 900 - i - 100);
                            but.Content = rooms[k].name;
                            but.Click += roomClicked;
                            but.Tag = rooms[k].id.ToString();
                            but.Background = Brushes.White;
                            but.FontSize = 40;
                            panel.Children.Add(but);
                        }
                    }
                    flag = 1;
                }
            }
            grid1.Children.Add(panel);
        }

        /*
        Switch to MainMenu Window
        input:  (button info) sender, e
        output: (void)
        */
        private void Back(object sender, RoutedEventArgs e)
        {
            background_worker.CancelAsync();
            MainMenu mainMenuWind = new MainMenu(client);
            mainMenuWind.Show();
            this.Close();
        }

        /*
        Tries to GetAllPlayewrsInRoom (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void roomClicked(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            string name = clicked.Tag.ToString();
            int id = Int32.Parse(name);
            current_id = id;

            GetPlayersInRoomReq getPlayerReq = new GetPlayersInRoomReq
            {
                roomId = id
            };

            string jsonString = JsonConvert.SerializeObject(getPlayerReq);
            string msg = Constants.GET_PLAYERS_IN_ROOM + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            GetPlayersInRoomRes getPlayerInRoom = JsonConvert.DeserializeObject<GetPlayersInRoomRes>(received.Substring(6));
            Players.Text = getPlayerInRoom.PlayersInRoom;
        }


        /*
        Tries to JoinRoom (Send To server)
        input:  (button info) sender, e
        output: (void)
        */
        private void Join(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Text = "";
            JoinRoomReq joinRoomReq = new JoinRoomReq()
            {
                roomId = current_id
            };
            if(current_id == -1) //if no room was chosen
            {
                ErrorMsg.Text = "First Choose a Room";
                return;
            }

            string jsonString = JsonConvert.SerializeObject(joinRoomReq); ;
            string msg = Constants.JOIN_ROOM + jsonString.Length.ToString().PadLeft(4, '0') + jsonString;

            this.client.send(msg);
            string received = this.client.receive();

            JoinRoomRes joinRoomRess = JsonConvert.DeserializeObject<JoinRoomRes>(received.Substring(6));
            if (joinRoomRess.status != 1)
            {
                ErrorMsg.Text = "Room is Full"; ;
            }
            else
            {
                RoomData roomDat = new RoomData();
                for(int i = 0; i < rooms.Length; i++)
                {
                    if (current_id == rooms[i].id)
                    {
                        roomDat = rooms[i];
                    }
                }
                background_worker.CancelAsync();
                RoomMenuWindow roomWind = new RoomMenuWindow(client, roomDat, 0);
                roomWind.Show();
                this.Close();
            }
        }
    }
}
